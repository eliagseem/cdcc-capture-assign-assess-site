﻿
Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If HttpContext.Current.User.Identity.IsAuthenticated Then

            HyperLink1.Text = "Change Password"
            HyperLink1.NavigateUrl = "~/PAGES/ALL/ChangePassword.aspx"
            Label1.Text = "Welcome, "

        Else

            HyperLink1.Text = ""
            Label1.Text = "Please log in here: "
        End If


    End Sub
End Class

