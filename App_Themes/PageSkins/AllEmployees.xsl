<?xml version="1.0" encoding="utf-8"?>
  <xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="EmployeeList">
      <h1>All Employees</h1>
      <xsl:for-each select="Employee" >
         <xsl:sort select="EmployeeName/LastName" data-type="text" />  
       

            <em><xsl:value-of select="EmployeeName/LastName"/>,
             <xsl:value-of select="EmployeeName/FirstName"/></em>: 
			 <strong><xsl:value-of select="Job/Title"/></strong><br />

      
       </xsl:for-each>
    </xsl:template>
    
  </xsl:stylesheet>