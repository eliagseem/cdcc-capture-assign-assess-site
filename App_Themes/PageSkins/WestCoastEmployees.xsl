<?xml version="1.0" encoding="utf-8"?>
  <xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="EmployeeList">
      <h1>West Coast Employees Only</h1>
      <xsl:for-each select="Employee" >
         <xsl:sort select="EmployeeName/LastName" data-type="text" />  
         <xsl:if test="Location='West Coast'">

            <em><xsl:value-of select="EmployeeName/LastName"/>,
             <xsl:value-of select="EmployeeName/FirstName"/></em>. Job title: 
			 <strong><xsl:value-of select="Job/Title"/></strong><br />

         </xsl:if>
       </xsl:for-each>
    </xsl:template>
    
  </xsl:stylesheet>

  