﻿Imports System.Data.OleDb
Partial Class PAGES_DIRECTOR_EditUser
    Inherits System.Web.UI.Page

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView1.SelectedIndexChanged
        Dim rowId As Integer = CInt(GridView1.DataKeys(GridView1.SelectedIndex).Value)

        'Dim mu As MembershipUser = Membership.GetUser(GridView1.SelectedRow.Cells(6).Text)

        '   mu.ChangePassword(mu.ResetPassword(), password)

        empName.Text = GridView1.SelectedRow.Cells(5).Text
        'password.Text = mu.GetPassword(GridView1.SelectedRow.Cells(6).Text).ToString
        location.Text = GridView1.SelectedRow.Cells(3).Text
        ddl_clearance.SelectedValue = GridView1.SelectedRow.Cells(4).Text
        txt_title.Text = GridView1.SelectedRow.Cells(2).Text
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()

        Dim myCommand As OleDbCommand
        Dim alterCmd As String


        alterCmd = "UPDATE Employee SET EmployeeName = @empname, Title = @title, Location = @loc, ClearanceLevel = @clev WHERE EmployeeID = " + GridView1.SelectedRow.Cells(1).Text + ";"
        myCommand = New OleDbCommand(alterCmd, conn)

        myCommand.Parameters.Add("@empname", OleDbType.VarChar)
        myCommand.Parameters("@empname").Value = empName.Text

        myCommand.Parameters.Add("@title", OleDbType.VarChar)
        myCommand.Parameters("@title").Value = txt_title.Text

        myCommand.Parameters.Add("@loc", OleDbType.VarChar)
        myCommand.Parameters("@loc").Value = location.Text

        myCommand.Parameters.Add("@clev", OleDbType.VarChar)
        myCommand.Parameters("@clev").Value = ddl_clearance.SelectedValue


        Try
            myCommand.ExecuteNonQuery()

            ' lbl_success.Text = "Inquiry successfully altered."
        Catch ex As OleDbException
            ' lbl_success.Text = "ERROR: Could not alter record, please ensure the fields are correctly filled out"
        End Try

        myCommand.Connection.Close()

        Roles.RemoveUserFromRole(GridView1.SelectedRow.Cells(6).Text, GridView1.SelectedRow.Cells(4).Text)
        Roles.AddUserToRole(GridView1.SelectedRow.Cells(6).Text, ddl_clearance.SelectedValue)

        Response.Redirect("EditUser.aspx")
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

    End Sub
End Class
