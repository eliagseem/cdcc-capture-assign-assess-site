﻿<%@ Page Title="CDCC CAA System" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditUser.aspx.vb" Inherits="PAGES_DIRECTOR_EditUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;"> 
        <h3 style="margin-left:0px;">
            EDIT USER</h3>
        <p style="margin-left:0px;">
        SeleSelect the row that you want to edit, then fill out the form and save your changes.<br /></p>
            <p style="margin-left:-20px;"><asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowSorting="True" DataKeyNames="EmployeeID" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" AllowPaging="True">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="EmployeeID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="EmployeeID" />
                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                <asp:BoundField DataField="ClearanceLevel" HeaderText="ClearanceLevel" SortExpression="ClearanceLevel" />
                <asp:BoundField DataField="EmployeeName" HeaderText="FullName" SortExpression="EmployeeName" />
                <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
            </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" DeleteCommand="DELETE FROM [Employee] WHERE [EmployeeID] = ?" InsertCommand="INSERT INTO [Employee] ([EmployeeID], [Title], [Location], [ClearanceLevel], [EmployeeName], [UserName]) VALUES (?, ?, ?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT * FROM [Employee]" UpdateCommand="UPDATE [Employee] SET [Title] = ?, [Location] = ?, [ClearanceLevel] = ?, [EmployeeName] = ?, [UserName] = ? WHERE [EmployeeID] = ?" FilterExpression="CONVERT([UserName], 'System.String') LIKE '{0}'">
            <DeleteParameters>
                <asp:Parameter Name="EmployeeID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="EmployeeID" Type="Int32" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Location" Type="String" />
                <asp:Parameter Name="ClearanceLevel" Type="String" />
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="UserName" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Location" Type="String" />
                <asp:Parameter Name="ClearanceLevel" Type="String" />
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="UserName" Type="String" />
                <asp:Parameter Name="EmployeeID" Type="Int32" />
            </UpdateParameters>

                                        <FilterParameters>
        <asp:ControlParameter Name="EmployeeID" ControlID="txt_filter" PropertyName="Text" />
    </FilterParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        Filter by Username: <asp:TextBox ID="txt_filter" runat="server"></asp:TextBox>
    &nbsp;<asp:Button ID="Button1" runat="server" Text="FILTER" CausesValidation="False" />
    </p>
     <p>
         &nbsp;</p>
    <p>
         Employee Name: <asp:TextBox ID="empName" runat="server"></asp:TextBox>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="empName" ErrorMessage="Name cannot be blank">Field is required.</asp:RequiredFieldValidator>
    </p>
    <p>
         Clearance Level: <asp:DropDownList ID="ddl_clearance" runat="server">
             <asp:ListItem>EMPLOYEE</asp:ListItem>
             <asp:ListItem>DIRECTOR</asp:ListItem>
             <asp:ListItem>SUPERVISOR</asp:ListItem>
         </asp:DropDownList>
    </p>
    <p>
         Office
         Location: <asp:TextBox ID="location" runat="server"></asp:TextBox>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="location" ErrorMessage="Location cannot be blank">Field is required.</asp:RequiredFieldValidator>
    </p>
    <p>
         Employee
         Title:&nbsp; <asp:TextBox ID="txt_title" runat="server"></asp:TextBox>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_title" ErrorMessage="Title cannot be blank">Field is required.</asp:RequiredFieldValidator>
    </p>
        <p>
            <asp:Button ID="Button2" runat="server" Text="Update User" />
    </p>
    <p>
         &nbsp;</p></section>
</asp:Content>

