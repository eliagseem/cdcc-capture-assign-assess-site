﻿
Partial Class PAGES_DIRECTOR_DeleteUser
    Inherits System.Web.UI.Page

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView1.SelectedIndexChanged

    End Sub

    Sub GridView1_RowDeleted(sender As Object, e As GridViewDeletedEventArgs)

    End Sub

    Sub CustomersGridView_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        Dim index As Integer = Convert.ToInt32(e.RowIndex)

        Dim userName As String = GridView1.Rows(index).Cells(3).Text

        Membership.DeleteUser(userName, True)

    End Sub

End Class
