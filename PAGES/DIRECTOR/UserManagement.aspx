﻿<%@ Page Title="CDCC CAA System" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserManagement.aspx.vb" Inherits="PAGES_DIRECTOR_UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <p style="min-height:400px;padding-bottom:24px;padding-left:50px;height:auto;margin-right:5px;margin-left:5px;width:700px;">
    <span style="font-size:1.2em;"><strong>USER MANAGEMENT (Directors only)</strong></span><br /><br />
			<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/IMAGES/buttons/createUser.png" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/IMAGES/buttons/editUser.png" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/IMAGES/buttons/delUser.png" />
    </p>

</asp:Content>

