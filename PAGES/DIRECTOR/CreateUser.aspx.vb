﻿Imports System.Data.OleDb
Imports System.Web.Configuration

Partial Class PAGES_PUBLIC_NewUser
    Inherits System.Web.UI.Page


    Protected Sub CreateUserWizard1_CreatedUser(sender As Object, e As EventArgs) Handles CreateUserWizard1.CreatedUser


        Dim empName As TextBox = TryCast(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("empName"), TextBox)
        Dim empTitle As TextBox = TryCast(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("empTitle"), TextBox)
        Dim clearanceLevel As DropDownList = TryCast(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("clearanceLevel"), DropDownList)
        Dim userName As TextBox = TryCast(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("UserName"), TextBox)
        Dim location As TextBox = TryCast(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("location"), TextBox)

        Dim conn As OleDbConnection

        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)

        conn.Open()
        Dim myCommand As OleDbCommand
        Dim insertCmd As String

        If (empName.Text = "" Or empTitle.Text = "") Then
            Exit Sub
        End If

        insertCmd = "INSERT into Employee(EmployeeName, Title, Location, ClearanceLevel, UserName) values(@name, @title, @loc, @clearance, @usrName);"
        myCommand = New OleDbCommand(insertCmd, conn)

        myCommand.Parameters.Add("@name", OleDbType.VarChar)
        myCommand.Parameters("@name").Value = empName.Text

        myCommand.Parameters.Add("@title", OleDbType.VarChar)
        myCommand.Parameters("@title").Value = empTitle.Text

        myCommand.Parameters.Add("@loc", OleDbType.VarChar)
        myCommand.Parameters("@loc").Value = location.Text

        myCommand.Parameters.Add("@clearance", OleDbType.VarChar)
        myCommand.Parameters("@clearance").Value = clearanceLevel.SelectedValue
        myCommand.Parameters.Add("@usrName", OleDbType.VarChar)
        myCommand.Parameters("@usrName").Value = userName.Text


        Try
            myCommand.ExecuteNonQuery()
            'lbl_success.Text = "Company created successfully!"
        Catch ex As OleDbException
            'lbl_success.Text = "ERROR: Could not add record, please ensure the fields are correctly filled out"
        End Try

        myCommand.Connection.Close()

        Dim isInRole As Boolean = System.Web.Security.Roles.IsUserInRole(userName.Text, clearanceLevel.SelectedValue)
        If isInRole = False Then
            Roles.AddUserToRole(userName.Text, clearanceLevel.SelectedValue)
        End If
    End Sub
End Class
