﻿<%@ Page Title="CDCC CAA System" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CreateUser.aspx.vb" Inherits="PAGES_PUBLIC_NewUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">



    <p>
        <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" Height="275px" Width="417px" ContinueDestinationPageUrl="~/PAGES/EMPLOYEES/HomePage.aspx" RequireEmail="False">
            <WizardSteps>
                <asp:CreateUserWizardStep runat="server" >
                    <ContentTemplate>
                        <table style="font-size: 100%; height: 275px; width: 412px;">
                            <tr>
                                <td align="center" colspan="2">Enter Desired User Credentials</td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name: </asp:Label>
                                </td>
                                <td class="auto-style1">
                                    <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                </td>
                                <td class="auto-style1">
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label>
                                </td>
                                <td class="auto-style1">
                                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Clearance Level</td>
                                <td class="auto-style1">
                                    <asp:DropDownList ID="clearanceLevel" runat="server">
                                        <asp:ListItem Selected="True">EMPLOYEE</asp:ListItem>
                                        <asp:ListItem>DIRECTOR</asp:ListItem>
                                        <asp:ListItem>SUPERVISOR</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Employee Name</td>
                                <td class="auto-style1">
                                    <asp:TextBox ID="empName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="empName" ErrorMessage="An employee name is required." ToolTip="An employee name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="auto-style4">Employee Title</td>
                                <td class="auto-style5">
                                    <asp:TextBox ID="empTitle" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="TitleRequired" runat="server" ControlToValidate="empTitle" ErrorMessage="An employee title is required." ToolTip="An employee title is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="auto-style2">Office Location</td>
                                <td class="auto-style3">
                                    <asp:TextBox ID="location" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="location" ErrorMessage="An office location is required">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="color: Red;">
                                    <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:CreateUserWizardStep>
                <asp:CompleteWizardStep runat="server" >
                    <ContentTemplate>
                        <table style="font-size:100%;height:275px;width:321px;">
                            <tr>
                                <td align="center" colspan="2">Complete</td>
                            </tr>
                            <tr>
                                <td>Your account has been successfully created.</td>
                            </tr>
                            <tr>
                                <td align="right" colspan="2">
                                    <asp:Button ID="ContinueButton" runat="server" CausesValidation="False" CommandName="Continue" Text="Continue" ValidationGroup="CreateUserWizard1" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:CompleteWizardStep>
            </WizardSteps>
            <StartNavigationTemplate>
                <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" Text="Next" />
            </StartNavigationTemplate>
        </asp:CreateUserWizard>
</p>



</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
    .auto-style1 {
        width: 156px;
    }
    .auto-style2 {
        height: 13px;
    }
    .auto-style3 {
        width: 156px;
        height: 13px;
    }
        .auto-style4 {
            height: 29px;
        }
        .auto-style5 {
            width: 156px;
            height: 29px;
        }
    </style>
</asp:Content>


