﻿<%@ Page Title="CDCC CAA System" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DeleteUser.aspx.vb" Inherits="PAGES_DIRECTOR_DeleteUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <h3 style="margin-left:50px;">
        DELETE USER</h3>
    <p style="margin-left:50px;">
        Select &quot;Delete&quot; on the row that you want to remove.<asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnRowDeleting="CustomersGridView_RowDeleting" DataKeyNames="EmployeeID" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
            <Columns>
                <asp:TemplateField HeaderText="Delete">
	                <ItemTemplate>
		                <asp:Button ID="deleteButton" runat="server" CommandName="Delete" Text="Delete"
                OnClientClick="return confirm('WARNING: Are you sure you want to delete this user record? This cannot be undone and will delete all this employee's information.');" />
	                </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="False" />
                <asp:BoundField DataField="EmployeeID" HeaderText="EmployeeID" InsertVisible="False" ReadOnly="True" SortExpression="EmployeeID" />
                <asp:BoundField DataField="EmployeeName" HeaderText="EmployeeName" SortExpression="EmployeeName" />
                <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
                <asp:BoundField DataField="ClearanceLevel" HeaderText="ClearanceLevel" SortExpression="ClearanceLevel" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" DeleteCommand="DELETE FROM [Employee] WHERE [EmployeeID] = ?" InsertCommand="INSERT INTO [Employee] ([EmployeeID], [EmployeeName], [UserName], [ClearanceLevel]) VALUES (?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT [EmployeeID], [EmployeeName], [UserName], [ClearanceLevel] FROM [Employee]" UpdateCommand="UPDATE [Employee] SET [EmployeeName] = ?, [UserName] = ?, [ClearanceLevel] = ? WHERE [EmployeeID] = ?" FilterExpression="CONVERT([EmployeeID], 'System.String') LIKE '{0}'">
            <DeleteParameters>
                <asp:Parameter Name="EmployeeID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="EmployeeID" Type="Int32" />
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="UserName" Type="String" />
                <asp:Parameter Name="ClearanceLevel" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="UserName" Type="String" />
                <asp:Parameter Name="ClearanceLevel" Type="String" />
                <asp:Parameter Name="EmployeeID" Type="Int32" />
            </UpdateParameters>
                            <FilterParameters>
        <asp:ControlParameter Name="EmployeeID" ControlID="txt_filter" PropertyName="Text" />
    </FilterParameters>
        </asp:SqlDataSource>
    </p>
    <p style="margin-left:50px;">
        Filter by Employee ID: <asp:TextBox ID="txt_filter" runat="server"></asp:TextBox>
    &nbsp;<asp:Button ID="Button1" runat="server" Text="FILTER" CausesValidation="False" />
    </p>
    <p style="height:300px;">
        &nbsp;</p>
</asp:Content>

