﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DelCompany.aspx.vb" Inherits="PAGES_DIRECTOR_DelCompany" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;"> <p>
        DELETE Organization: Select &quot;Delete&quot; on the row that you want to remove.<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="CompanyID" DataSourceID="SqlDataSource1" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
            <Columns>
                <asp:TemplateField HeaderText="Delete">
	                <ItemTemplate>
		                <asp:Button ID="deleteButton" runat="server" CommandName="Delete" Text="Delete"
                OnClientClick="return confirm('Are you sure you want to delete this company record? This cannot be undone.');" />
	                </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="False" />
                <asp:BoundField DataField="CompanyID" HeaderText="OrganizationID" InsertVisible="False" ReadOnly="True" SortExpression="CompanyID" />
                <asp:BoundField DataField="CompanyName" HeaderText="Name" SortExpression="CompanyName" />
                <asp:BoundField DataField="CompanyAddress" HeaderText="Address" SortExpression="CompanyAddress" />
                <asp:BoundField DataField="CompanyTelephone" HeaderText="Telephone" SortExpression="CompanyTelephone" />
                <asp:BoundField DataField="Industry" HeaderText="Industry" SortExpression="Industry" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" DeleteCommand="DELETE FROM [Company] WHERE [CompanyID] = ?" InsertCommand="INSERT INTO [Company] ([CompanyID], [CompanyName], [CompanyAddress], [CompanyTelephone], [Industry]) VALUES (?, ?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT * FROM [Company]" UpdateCommand="UPDATE [Company] SET [CompanyName] = ?, [CompanyAddress] = ?, [CompanyTelephone] = ?, [Industry] = ? WHERE [CompanyID] = ?" FilterExpression="CONVERT([CompanyID], 'System.String') LIKE '{0}'">
            <DeleteParameters>
                <asp:Parameter Name="CompanyID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="CompanyID" Type="Int32" />
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="CompanyAddress" Type="String" />
                <asp:Parameter Name="CompanyTelephone" Type="String" />
                <asp:Parameter Name="Industry" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="CompanyAddress" Type="String" />
                <asp:Parameter Name="CompanyTelephone" Type="String" />
                <asp:Parameter Name="Industry" Type="String" />
                <asp:Parameter Name="CompanyID" Type="Int32" />
            </UpdateParameters>
                            <FilterParameters>
        <asp:ControlParameter Name="CompanyID" ControlID="txt_filter" PropertyName="Text" />
    </FilterParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        Filter by Organization ID:
        <asp:TextBox ID="txt_filter" runat="server"></asp:TextBox>
    &nbsp; <asp:Button ID="Button4" runat="server" Text="FILTER" />
        </p>
        <p>
            &nbsp;</p></section>
</asp:Content>

