﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DelCustomer.aspx.vb" Inherits="PAGES_DIRECTOR_DelCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;"> <p>
        DELETE CUSTOMER: Select &quot;Delete&quot; on the row that you want to remove.<asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="CustomerID" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" AllowPaging="True">
            <Columns>
            <asp:TemplateField HeaderText="Delete">
	                <ItemTemplate>
		                <asp:Button ID="deleteButton" runat="server" CommandName="Delete" Text="Delete"
                OnClientClick="return confirm('Are you sure you want to delete this customer record? This cannot be undone. Related records will be cascade deleted.');" />
	                </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="False" />
                <asp:BoundField DataField="CustomerID" HeaderText="Customer ID" InsertVisible="False" ReadOnly="True" SortExpression="CustomerID" />
                <asp:BoundField DataField="CustomerFName" HeaderText="First Name" SortExpression="CustomerFName" />
                <asp:BoundField DataField="CustomerLName" HeaderText="Last Name" SortExpression="CustomerLName" />
                <asp:BoundField DataField="CustomerTelephone" HeaderText="Telephone" SortExpression="CustomerTelephone" />
                <asp:BoundField DataField="CustomerEmail" HeaderText="Email" SortExpression="CustomerEmail" />
                <asp:BoundField DataField="CompanyID" HeaderText="Organization ID" SortExpression="CompanyID" />
                <asp:BoundField DataField="CustomerIPAddress" HeaderText="IP Address" SortExpression="CustomerIPAddress" />
                <asp:BoundField DataField="CustomerWebsite" HeaderText="Website" SortExpression="CustomerWebsite" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" DeleteCommand="DELETE FROM [Customer] WHERE [CustomerID] = ?" InsertCommand="INSERT INTO [Customer] ([CustomerID], [CustomerFName], [CustomerLName], [CustomerTelephone], [CustomerEmail], [CompanyID]) VALUES (?, ?, ?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT * FROM [Customer]" UpdateCommand="UPDATE [Customer] SET [CustomerFName] = ?, [CustomerLName] = ?, [CustomerTelephone] = ?, [CustomerEmail] = ?, [CompanyID] = ? WHERE [CustomerID] = ?" FilterExpression="CONVERT([CustomerID], 'System.String') LIKE '{0}'">
            <DeleteParameters>
                <asp:Parameter Name="CustomerID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="CustomerID" Type="Int32" />
                <asp:Parameter Name="CustomerFName" Type="String" />
                <asp:Parameter Name="CustomerLName" Type="String" />
                <asp:Parameter Name="CustomerTelephone" Type="String" />
                <asp:Parameter Name="CustomerEmail" Type="String" />
                <asp:Parameter Name="CompanyID" Type="Int32" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="CustomerFName" Type="String" />
                <asp:Parameter Name="CustomerLName" Type="String" />
                <asp:Parameter Name="CustomerTelephone" Type="String" />
                <asp:Parameter Name="CustomerEmail" Type="String" />
                <asp:Parameter Name="CompanyID" Type="Int32" />
                <asp:Parameter Name="CustomerID" Type="Int32" />
            </UpdateParameters>

                <FilterParameters>
        <asp:ControlParameter Name="CustomerID" ControlID="txt_filter" PropertyName="Text" />
    </FilterParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        Filter by Customer ID:
        <asp:TextBox ID="txt_filter" runat="server"></asp:TextBox>
    &nbsp;<asp:Button ID="Button4" runat="server" Text="FILTER" />
    </p></section>
</asp:Content>

