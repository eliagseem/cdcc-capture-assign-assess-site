﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DelRequest.aspx.vb" Inherits="PAGES_DIRECTOR_DelRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;">  <p>
        DELETE REQUEST: Select &quot;Delete&quot; on the row that you want to remove.<asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="RequestID" DataSourceID="SqlDataSource1" AllowPaging="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" PageSize="4" Width="897px">
            <Columns>
                 <asp:TemplateField HeaderText="Delete">
	                <ItemTemplate>
		                <asp:Button ID="deleteButton" runat="server" CommandName="Delete" Text="Delete"
                OnClientClick="return confirm('Are you sure you want to delete this request? Any related records, including In Progress and Resolved request info will be deleted as well.');" />
	                </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="RequestID" HeaderText="RequestID" InsertVisible="False" ReadOnly="True" SortExpression="RequestID" />
                <asp:BoundField DataField="Frequency" HeaderText="Frequency" SortExpression="Frequency" />
                <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
                <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
                <asp:BoundField DataField="Topic" HeaderText="Topic" SortExpression="Topic" />
                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" DeleteCommand="DELETE FROM [Request] WHERE [RequestID] = ? AND (([Frequency] = ?) OR ([Frequency] IS NULL AND ? IS NULL)) AND (([Details] = ?) OR ([Details] IS NULL AND ? IS NULL)) AND (([DateCreated] = ?) OR ([DateCreated] IS NULL AND ? IS NULL)) AND (([Topic] = ?) OR ([Topic] IS NULL AND ? IS NULL)) AND (([Status] = ?) OR ([Status] IS NULL AND ? IS NULL))" InsertCommand="INSERT INTO [Request] ([RequestID], [Frequency], [Details], [DateCreated], [Topic], [Status]) VALUES (?, ?, ?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT * FROM [Request]" UpdateCommand="UPDATE [Request] SET [Frequency] = ?, [Details] = ?, [DateCreated] = ?, [Topic] = ?, [Status] = ? WHERE [RequestID] = ? AND (([Frequency] = ?) OR ([Frequency] IS NULL AND ? IS NULL)) AND (([Details] = ?) OR ([Details] IS NULL AND ? IS NULL)) AND (([DateCreated] = ?) OR ([DateCreated] IS NULL AND ? IS NULL)) AND (([Topic] = ?) OR ([Topic] IS NULL AND ? IS NULL)) AND (([Status] = ?) OR ([Status] IS NULL AND ? IS NULL))" FilterExpression="CONVERT([RequestID], 'System.String') LIKE '{0}'" ConflictDetection="CompareAllValues" OldValuesParameterFormatString="original_{0}">
            <DeleteParameters>
                <asp:Parameter Name="original_RequestID" Type="Int32" />
                <asp:Parameter Name="original_Frequency" Type="String" />
                <asp:Parameter Name="original_Frequency" Type="String" />
                <asp:Parameter Name="original_Details" Type="String" />
                <asp:Parameter Name="original_Details" Type="String" />
                <asp:Parameter Name="original_DateCreated" Type="DateTime" />
                <asp:Parameter Name="original_DateCreated" Type="DateTime" />
                <asp:Parameter Name="original_Topic" Type="String" />
                <asp:Parameter Name="original_Topic" Type="String" />
                <asp:Parameter Name="original_Status" Type="String" />
                <asp:Parameter Name="original_Status" Type="String" />
            </DeleteParameters>
                <FilterParameters>
        <asp:ControlParameter Name="RequestID" ControlID="txt_filter" PropertyName="Text" />
    </FilterParameters>
            <InsertParameters>
                <asp:Parameter Name="RequestID" Type="Int32" />
                <asp:Parameter Name="Frequency" Type="String" />
                <asp:Parameter Name="Details" Type="String" />
                <asp:Parameter Name="DateCreated" Type="DateTime" />
                <asp:Parameter Name="Topic" Type="String" />
                <asp:Parameter Name="Status" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Frequency" Type="String" />
                <asp:Parameter Name="Details" Type="String" />
                <asp:Parameter Name="DateCreated" Type="DateTime" />
                <asp:Parameter Name="Topic" Type="String" />
                <asp:Parameter Name="Status" Type="String" />
                <asp:Parameter Name="original_RequestID" Type="Int32" />
                <asp:Parameter Name="original_Frequency" Type="String" />
                <asp:Parameter Name="original_Frequency" Type="String" />
                <asp:Parameter Name="original_Details" Type="String" />
                <asp:Parameter Name="original_Details" Type="String" />
                <asp:Parameter Name="original_DateCreated" Type="DateTime" />
                <asp:Parameter Name="original_DateCreated" Type="DateTime" />
                <asp:Parameter Name="original_Topic" Type="String" />
                <asp:Parameter Name="original_Topic" Type="String" />
                <asp:Parameter Name="original_Status" Type="String" />
                <asp:Parameter Name="original_Status" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        Filter by Request ID:
        <asp:TextBox ID="txt_filter" runat="server" TextMode="Number"></asp:TextBox>
    &nbsp;<asp:Button ID="Button4" runat="server" Text="FILTER" />
    </p></section>
</asp:Content>

