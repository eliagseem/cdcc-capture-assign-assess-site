﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DelInquiry.aspx.vb" Inherits="PAGES_DIRECTOR_DelInquiry" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;">  <p>
        DELETE INQUIRY: Select &quot;Delete&quot; on the row that you want to remove.<asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="InquiryID" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" AllowPaging="True">
            <Columns>
                <asp:TemplateField HeaderText="Delete">
	                <ItemTemplate>
		                <asp:Button ID="deleteButton" runat="server" CommandName="Delete" Text="Delete"
                OnClientClick="return confirm('Are you sure you want to delete this inquiry? This step cannot be undone!');" />
	                </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="False" />
                <asp:BoundField DataField="InquiryID" HeaderText="InquiryID" InsertVisible="False" ReadOnly="True" SortExpression="InquiryID" />
                <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
                <asp:BoundField DataField="EmployeeID" HeaderText="EmployeeID" SortExpression="EmployeeID" />
                <asp:BoundField DataField="CustomerID" HeaderText="CustomerID" SortExpression="CustomerID" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />

        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" DeleteCommand="DELETE FROM [Inquiry] WHERE [InquiryID] = ?" InsertCommand="INSERT INTO [Inquiry] ([InquiryID], [DateCreated], [EmployeeID], [CustomerID]) VALUES (?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT * FROM [Inquiry]" UpdateCommand="UPDATE [Inquiry] SET [DateCreated] = ?, [EmployeeID] = ?, [CustomerID] = ? WHERE [InquiryID] = ?" FilterExpression="CONVERT([InquiryID], 'System.String') LIKE '{0}'">
            <DeleteParameters>
                <asp:Parameter Name="InquiryID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="InquiryID" Type="Int32" />
                <asp:Parameter Name="DateCreated" Type="DateTime" />
                <asp:Parameter Name="EmployeeID" Type="Int32" />
                <asp:Parameter Name="CustomerID" Type="Int32" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="DateCreated" Type="DateTime" />
                <asp:Parameter Name="EmployeeID" Type="Int32" />
                <asp:Parameter Name="CustomerID" Type="Int32" />
                <asp:Parameter Name="InquiryID" Type="Int32" />
            </UpdateParameters>
                            <FilterParameters>
        <asp:ControlParameter Name="InquiryID" ControlID="txt_filter" PropertyName="Text" />
    </FilterParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        Filter by Inquiry ID:
        <asp:TextBox ID="txt_filter" runat="server"></asp:TextBox>
    &nbsp;<asp:Button ID="Button4" runat="server" Text="FILTER" />
    </p></section>
</asp:Content>

