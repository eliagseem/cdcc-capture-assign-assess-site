﻿
Partial Class PAGES_EMPLOYEES_InquiryManagement
    Inherits System.Web.UI.Page

    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click
        Response.Redirect("EditInquiry.aspx")
    End Sub

    Protected Sub ImageButton2_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton2.Click
        Response.Redirect("../SUPERVISOR/DelInquiry.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If (HttpContext.Current.User.Identity.IsAuthenticated) Then
            If (User.IsInRole("EMPLOYEE")) Then
                ImageButton2.Visible = False
            End If
        End If
    End Sub
End Class
