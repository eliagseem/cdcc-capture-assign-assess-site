﻿Imports System.Data.OleDb
Imports System.Web.Configuration

Partial Class PAGES_EMPLOYEES_CreateRequest
    Inherits System.Web.UI.Page


    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()

        Dim myCommand As OleDbCommand
        Dim insertCmd As String

        If (txt_topic.Text = "" Or txt_details.Text = "") Then
            Exit Sub
        End If

        insertCmd = "INSERT into Request(Topic, DateCreated, Details, Status, Frequency) values(@topic, @date, @details, @status, @frequency);"
        myCommand = New OleDbCommand(insertCmd, conn)

        myCommand.Parameters.Add("@topic", OleDbType.VarChar)
        myCommand.Parameters("@topic").Value = txt_topic.Text

        myCommand.Parameters.Add("@date", OleDbType.Date)
        myCommand.Parameters("@date").Value = Calendar1.SelectedDate.ToShortDateString + " " + DateTime.Now.ToShortTimeString

        myCommand.Parameters.Add("@details", OleDbType.VarChar)
        myCommand.Parameters("@details").Value = txt_details.Text

        myCommand.Parameters.Add("@status", OleDbType.VarChar)
        myCommand.Parameters("@status").Value = "Open"

        myCommand.Parameters.Add("@frequency", OleDbType.VarChar)
        myCommand.Parameters("@frequency").Value = "Rare"


        Try
            myCommand.ExecuteNonQuery()
            lbl_success.Text = "Request successfully created. You can now edit with status-specific fields on the Edit Request page."
        Catch ex As OleDbException
            lbl_success.Text = "ERROR: Could not add record, please ensure the fields are correctly filled out"
        End Try

        myCommand.Connection.Close()
    End Sub


    Protected Sub Calendar1_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar1.SelectionChanged
        TextBox1.Text = Calendar1.SelectedDate.ToShortDateString
        TextBox1.Text += " " + DateTime.Now.ToShortTimeString
    End Sub

    Private Sub BindCalendar()
        Calendar1.SelectedDate = DateTime.Now
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        BindCalendar()
    End Sub
End Class
