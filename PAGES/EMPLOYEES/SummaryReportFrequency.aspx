﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SummaryReportFrequency.aspx.vb" Inherits="PAGES_EMPLOYEES_StatusReport" MaintainScrollPositionOnPostBack="True"%>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
        <script type = "text/javascript">
            function PrintPanel() {
                var panel = document.getElementById("<%=pnlContents.ClientID %>");
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write('<html><head><title>DIV Contents</title>');
                printWindow.document.write('</head><body >');
                printWindow.document.write(panel.innerHTML);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                setTimeout(function () {
                    printWindow.print();
                }, 500);
                return false;
            }
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;"> 
    <asp:Panel ID="pnlContents" runat="server">
        <p>
       <h3>FREQUENCY SUMMARY REPORT</h3>
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="638px">
                <LocalReport ReportPath="PAGES\EMPLOYEES\Reports\FrequencyReport.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="FreqDS" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT [RequestID], [Frequency], [Details], [DateCreated], [Status], [Topic] FROM [Request] ORDER BY [RequestID]"
                FilterExpression="Frequency='{0}'">
                <FilterParameters>
                    <asp:ControlParameter Name="Frequency" ControlId="RadioButtonList1" PropertyName="SelectedValue"/>
                </FilterParameters></asp:SqlDataSource>
            <p>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </p>
    </p>
        </asp:Panel>
        Filter by Frequency (Rare, Periodical or Frequent):<br />
        <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True">
            <asp:ListItem>Rare</asp:ListItem>
            <asp:ListItem>Periodical</asp:ListItem>
            <asp:ListItem>Frequent</asp:ListItem>
        </asp:RadioButtonList>
        &nbsp;&nbsp;&nbsp;<br /><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/PAGES/EMPLOYEES/SummaryReportFrequency.aspx">Show full table and remove filters</asp:HyperLink>
    <p>
        &nbsp;</p></section>
</asp:Content>

