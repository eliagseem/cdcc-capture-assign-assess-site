﻿Imports System.Data.OleDb
Imports System.Web.Configuration
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Partial Class PAGES_EMPLOYEES_EditInquiry
    Inherits System.Web.UI.Page

    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

    End Sub

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()

        Dim myCommand As OleDbCommand
        Dim alterCmd As String


        alterCmd = "UPDATE Inquiry SET DateCreated = @date, EmployeeID = @empid, CustomerID = @custid WHERE InquiryID = " + GridView1.SelectedRow.Cells(1).Text + ";"
        myCommand = New OleDbCommand(alterCmd, conn)

        myCommand.Parameters.Add("@date", OleDbType.Date)
        myCommand.Parameters("@date").Value = TextBox2.Text

        myCommand.Parameters.Add("@empid", OleDbType.VarChar)
        myCommand.Parameters("@empid").Value = DropDownList2.SelectedValue

        myCommand.Parameters.Add("@custid", OleDbType.Integer)
        myCommand.Parameters("@custid").Value = CInt(DropDownList3.SelectedValue)

        Try
            myCommand.ExecuteNonQuery()
            lbl_success.Text = "Inquiry successfully altered."
        Catch ex As OleDbException
            lbl_success.Text = "ERROR: Could not alter record, please ensure the fields are correctly filled out"
        End Try

        myCommand.Connection.Close()
        'Update inquiry_request table
        Dim conn2 As OleDbConnection
        conn2 = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn2.Open()


        Dim myCommand2 As OleDbCommand
        Dim alterCmd2 As String


        alterCmd2 = "UPDATE Inquiry_Request SET RequestID=@requestID WHERE InquiryID = " + GridView1.SelectedRow.Cells(1).Text + ";"
        myCommand2 = New OleDbCommand(alterCmd2, conn2)

        myCommand2.Parameters.Add("@requestID", OleDbType.Integer)
        myCommand2.Parameters("@requestID").Value = DropDownList4.SelectedValue

        Try
            myCommand2.ExecuteNonQuery()
        Catch ex As OleDbException
            lbl_success.Text = "ERROR: Back-end error editing inquiries."
        End Try

        myCommand2.Connection.Close()
    End Sub

    Protected Sub Calendar1_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar1.SelectionChanged
        TextBox2.Text = Calendar1.SelectedDate.ToShortDateString
        TextBox2.Text += " " + DateTime.Now.ToShortTimeString
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView1.SelectedIndexChanged
        Dim rowId As Integer = CInt(GridView1.DataKeys(GridView1.SelectedIndex).Value)
        SqlDataSource6.FilterExpression = "InquiryID = " & rowId
        GridView2.Visible = True
        TextBox2.Text = GridView1.SelectedRow.Cells(2).Text
        DropDownList2.SelectedValue = GridView1.SelectedRow.Cells(3).Text
        DropDownList3.SelectedValue = GridView1.SelectedRow.Cells(4).Text

            DropDownList4.SelectedValue = GridView2.Rows(0).Cells(1).Text


    End Sub
End Class
