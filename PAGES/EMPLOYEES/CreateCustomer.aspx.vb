﻿Imports System.Data.OleDb
Imports System.Web.Configuration

Partial Class PAGES_EMPLOYEES_CreateCustomer
    Inherits System.Web.UI.Page

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()

        Dim myCommand As OleDbCommand
        Dim insertCmd As String

        If (txt_fname.Text = "" Or txt_lname.Text = "" Or txt_phone.Text = "" Or IsNumeric(txt_phone.Text) = False) Then
            lbl_success.Text = "Telephone number must be numeric!"
            Exit Sub
        End If

        insertCmd = "INSERT into Customer(CustomerFName, CustomerLName, CustomerTelephone, CustomerEmail, CompanyID, CustomerIPAddress, CustomerWebsite) values(@fname, @lname, @phone, @email, @compid, @ip, @site);"
        myCommand = New OleDbCommand(insertCmd, conn)

        myCommand.Parameters.Add("@fname", OleDbType.VarChar)
        myCommand.Parameters("@fname").Value = txt_fname.Text

        myCommand.Parameters.Add("@lname", OleDbType.VarChar)
        myCommand.Parameters("@lname").Value = txt_lname.Text

        myCommand.Parameters.Add("@phone", OleDbType.VarChar)
        myCommand.Parameters("@phone").Value = txt_phone.Text

        myCommand.Parameters.Add("@email", OleDbType.VarChar)
        myCommand.Parameters("@email").Value = txt_email.Text

        myCommand.Parameters.Add("@compid", OleDbType.Integer)
        myCommand.Parameters("@compid").Value = ddl_companies.SelectedValue

        myCommand.Parameters.Add("@ip", OleDbType.VarChar)
        myCommand.Parameters("@ip").Value = txtIPAddress.Text

        myCommand.Parameters.Add("@site", OleDbType.VarChar)
        myCommand.Parameters("@site").Value = txtWebsite.Text

        Try
            lbl_success.Text = "Customer successfully created!"
            myCommand.ExecuteNonQuery()
        Catch ex As OleDbException
            lbl_success.Text = "ERROR: Could not add record, please ensure the fields are correctly filled out"
        End Try

        myCommand.Connection.Close()
    End Sub

    Function getCompanyId(x As String) As Integer
        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()
        Dim commString = "SELECT CompanyID FROM Company WHERE CompanyName = '" + x + "';"
        Dim comm = New OleDbCommand(commString, conn)



        Dim odr As OleDbDataReader = comm.ExecuteReader

        Dim compId As String = ""

        While (odr.Read())
            compId = odr("CompanyID").ToString
        End While

        comm.Connection.Close()

        Return CInt(compId)
    End Function

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ddl_companies.DataBind()

    End Sub
End Class
