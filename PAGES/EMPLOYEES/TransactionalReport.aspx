﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="TransactionalReport.aspx.vb" Inherits="PAGES_EMPLOYEES_TransactionalReport" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
        <script type = "text/javascript">
            function PrintPanel() {
                var panel = document.getElementById("<%=pnlContents.ClientID %>");
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write('<html><head><title>Transactional Report</title>');
                printWindow.document.write('</head><body >');
                printWindow.document.write(panel.innerHTML);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                setTimeout(function () {
                    printWindow.print();
                }, 500);
                return false;
            }
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;">  
       <asp:Panel ID="pnlContents" runat="server"> 
       <p>
      <h3>TRANSACTIONAL REPORT</h3>  
           <rsweb:ReportViewer ID="ReportViewer1" runat="server" BackColor="White" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="792px">
               <LocalReport ReportPath="PAGES\EMPLOYEES\Reports\TransactionalReport.rdlc">
                   <DataSources>
                       <rsweb:ReportDataSource DataSourceId="SqlDataSource2" Name="TransactionalDS" />
                   </DataSources>
               </LocalReport>
           </rsweb:ReportViewer>
           <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT        Request.RequestID, Request.Frequency, Request.Details, Request.DateCreated, Request.Topic, Request.Status, In_Progress_Request.EstimatedDeadline, 
                         In_Progress_Request.DateAssigned, In_Progress_Request.CustomerFeedback, Resolved_Request.DateResolved, Resolved_Request.ResolutionSatisfaction, 
                         In_Progress_Request.EmployeeID, In_Progress_Request.DepartmentID, Employee.EmployeeName, Department.DepartmentName
FROM            (((In_Progress_Request LEFT OUTER JOIN
                         Department ON In_Progress_Request.DepartmentID = Department.DepartmentID) LEFT OUTER JOIN
                         Employee ON In_Progress_Request.EmployeeID = Employee.EmployeeID) RIGHT OUTER JOIN
                         (Request LEFT OUTER JOIN
                         Resolved_Request ON Request.RequestID = Resolved_Request.RequestID) ON In_Progress_Request.RequestID = Request.RequestID)
ORDER BY Request.RequestID"></asp:SqlDataSource>
           <asp:ScriptManager ID="ScriptManager1" runat="server">
           </asp:ScriptManager>
           <p>
           </p>
    </p>
        </asp:Panel>
        </section>
</asp:Content>

