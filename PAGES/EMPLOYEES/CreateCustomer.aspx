﻿<%@ Page Title="CDCC CAA System" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CreateCustomer.aspx.vb" Inherits="PAGES_EMPLOYEES_CreateCustomer" MaintainScrollPositionOnPostBack="True"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <h2 style="text-align:center;">
        CREATE CUSTOMER</h2>
     <p style="margin-left:50px;">
        First Name:&nbsp;
        <asp:TextBox ID="txt_fname" runat="server" Width="128px"></asp:TextBox>
&nbsp;&nbsp; 

        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_fname" ErrorMessage="Field Cannot be left blank!" ForeColor="#FF3300"></asp:RequiredFieldValidator>

        <br />Last Name:&nbsp;
        <asp:TextBox ID="txt_lname" runat="server"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_lname" ErrorMessage="Field Cannot be left blank!" ForeColor="#FF3300"></asp:RequiredFieldValidator>
<br />
        Telephone: &nbsp;
        <asp:TextBox ID="txt_phone" runat="server" Width="128px"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_phone" ErrorMessage="Field Cannot be left blank!" ForeColor="#FF3300"></asp:RequiredFieldValidator>
         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txt_phone" ErrorMessage="Invalid telephone number entered" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" ForeColor="#FF3300"></asp:RegularExpressionValidator>
<br />
         <strong style="color:black">Format:&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 1234567891 | [Area code without seperator][Local Number without seperator]<br /></strong>
        Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
        <asp:TextBox ID="txt_email" runat="server" Width="128px"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txt_email" ErrorMessage="Field Cannot be left blank!" ForeColor="#FF3300"></asp:RequiredFieldValidator>
&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_email" ErrorMessage="Invalid email entered (follow someone@web.com format)" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="#FF3300"></asp:RegularExpressionValidator>
<br />
         IP Address:&nbsp;
        <asp:TextBox ID="txtIPAddress" runat="server" Width="128px"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtIPAddress" ErrorMessage="Field Cannot be left blank!" ForeColor="#FF3300"></asp:RequiredFieldValidator>
&nbsp;
         <br />
         Website:&nbsp;&nbsp;&nbsp; &nbsp;
        <asp:TextBox ID="txtWebsite" runat="server" Width="128px"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtWebsite" ErrorMessage="Field Cannot be left blank!" ForeColor="#FF3300"></asp:RequiredFieldValidator>
&nbsp;
         <br />
         Organization:<asp:DropDownList ID="ddl_companies" runat="server" DataSourceID="SqlDataSource1" DataTextField="CompanyName" DataValueField="CompanyID">
        </asp:DropDownList>
         <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [CompanyName], [CompanyID] FROM [Company]"></asp:SqlDataSource>
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/PAGES/EMPLOYEES/CreateCompany.aspx" Target="_blank " Font-Size="Medium">(Click here to create a new organization)</asp:HyperLink>
&nbsp;
        </p>
<p style="margin-left:50px;">
        <asp:Button ID="Button3" runat="server" Text="Refresh List" Width="85px" />
        </p>
    <p style="margin-left:50px;">
        <asp:Label ID="lbl_success" runat="server" ForeColor="#009933"></asp:Label>
<br /><br />
        <asp:Button ID="Button2" runat="server" Text="Create Customer" />
    </p>
    <p style="margin-left:50px;">
        &nbsp;</p>
</asp:Content>

