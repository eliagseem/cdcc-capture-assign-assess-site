﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SummaryReportResolving.aspx.vb" Inherits="PAGES_EMPLOYEES_StatusReport" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
        <script type = "text/javascript">
            function PrintPanel() {
                var panel = document.getElementById("<%=pnlContents.ClientID %>");
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write('<html><head><title>DIV Contents</title>');
                printWindow.document.write('</head><body >');
                printWindow.document.write(panel.innerHTML);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                setTimeout(function () {
                    printWindow.print();
                }, 500);
                return false;
            }
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;"> 
        <asp:Panel ID="pnlContents" runat="server">

        <p>
       <h3>REQUEST COMPLETION TIME REPORT</h3>
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="704px" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                <LocalReport ReportPath="PAGES\EMPLOYEES\Reports\CompletionReport.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="ResolvedDS" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT Resolved_Request.ResolvedReqID, In_Progress_Request.DateAssigned, Resolved_Request.DateResolved, Fix(((DATEDIFF('n', In_Progress_Request.DateAssigned, Resolved_Request.DateResolved) / 60 / 24))) &amp; ' days, ' &amp; ((DATEDIFF('n', In_Progress_Request.DateAssigned, Resolved_Request.DateResolved) / 60) Mod 24) &amp; ' hours and ' &amp; (DATEDIFF('n', In_Progress_Request.DateAssigned, Resolved_Request.DateResolved) Mod 60) &amp; ' minutes' AS [TimeSpentResolving], Resolved_Request.SpecialNotes, Resolved_Request.ResolutionSatisfaction, Resolved_Request.RequestID, Request.RequestID FROM ((Resolved_Request INNER JOIN Request ON Resolved_Request.RequestID = Request.RequestID) INNER JOIN In_Progress_Request ON Request.RequestID = In_Progress_Request.RequestID) ORDER BY Resolved_Request.ResolvedReqID"></asp:SqlDataSource>
            <p>
            </p>
            <p>
            </p>
    </p>
        </asp:Panel>
    <p>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </p></section>
</asp:Content>

