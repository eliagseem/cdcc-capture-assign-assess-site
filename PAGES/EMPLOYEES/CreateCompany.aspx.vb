﻿Imports System.Data.OleDb
Imports System.Web.Configuration

Partial Class PAGES_EMPLOYEES_CreateCompany
    Inherits System.Web.UI.Page

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim conn As OleDbConnection

        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)

        conn.Open()
        Dim myCommand As OleDbCommand
        Dim insertCmd As String

        If (txt_name.Text = "" Or txt_address.Text = "" Or txt_phone.Text = "" Or txt_industry.Text = "" Or IsNumeric(txt_phone.Text) = False) Then
            lbl_success.ForeColor = Drawing.Color.Red
            lbl_success.Text = "Telephone number must be numeric!"
            Exit Sub
        End If

        insertCmd = "INSERT into Company(CompanyName, CompanyAddress, CompanyTelephone, Industry) values(@name, @address, @phone, @industry);"
        myCommand = New OleDbCommand(insertCmd, conn)

        myCommand.Parameters.Add("@name", OleDbType.VarChar)
        myCommand.Parameters("@name").Value = txt_name.Text

        myCommand.Parameters.Add("@address", OleDbType.VarChar)
        myCommand.Parameters("@address").Value = txt_address.Text

        myCommand.Parameters.Add("@phone", OleDbType.VarChar)
        myCommand.Parameters("@phone").Value = txt_phone.Text

        myCommand.Parameters.Add("@industry", OleDbType.VarChar)
        myCommand.Parameters("@industry").Value = txt_industry.Text


        Try
            myCommand.ExecuteNonQuery()
            lbl_success.ForeColor = Drawing.Color.Green
            lbl_success.Text = "External Organization created successfully!"
        Catch ex As OleDbException
            lbl_success.ForeColor = Drawing.Color.Red

            lbl_success.Text = "ERROR: Could not add record, please ensure the fields are correctly filled out"
        End Try

        myCommand.Connection.Close()

    End Sub
End Class
