﻿Imports System.Data.OleDb
Imports System.Web.Configuration
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Partial Class PAGES_EMPLOYEES_EditCustomer
    Inherits System.Web.UI.Page

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView1.SelectedIndexChanged
        Dim rowId As Integer = CInt(GridView1.DataKeys(GridView1.SelectedIndex).Value)
        SqlDataSource1.FilterExpression = "CustomerID = " & rowId

        DropDownList1.SelectedValue = GridView1.SelectedRow.Cells(9).Text

        Dim currentCustID As String = GridView1.SelectedRow.Cells(1).Text
        TextBox5.Text = GridView1.SelectedRow.Cells(2).Text
        TextBox2.Text = GridView1.SelectedRow.Cells(3).Text
        TextBox3.Text = GridView1.SelectedRow.Cells(4).Text
        TextBox4.Text = GridView1.SelectedRow.Cells(5).Text
        txtIPAddress.Text = GridView1.SelectedRow.Cells(6).Text
        txtWebsite.Text = GridView1.SelectedRow.Cells(7).Text

        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()

        Dim myCommand As OleDbCommand
        Dim SelectCmd As String


        SelectCmd = "SELECT RequestID from Inquiry_Request WHERE InquiryID = " + currentCustID + ";"
        myCommand = New OleDbCommand(SelectCmd, conn)

    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()

        Dim myCommand As OleDbCommand
        Dim alterCmd As String


        alterCmd = "UPDATE Customer SET CustomerFName = @fname, CustomerLName = @lname, CustomerTelephone = @tel, CustomerEmail = @email, CompanyID = @companyid, CustomerIPAddress = @ip, CustomerWebsite = @site WHERE CustomerID = " + GridView1.SelectedRow.Cells(1).Text + ";"
        myCommand = New OleDbCommand(alterCmd, conn)

        myCommand.Parameters.Add("@fname", OleDbType.VarChar)
        myCommand.Parameters("@fname").Value = TextBox5.Text

        myCommand.Parameters.Add("@lname", OleDbType.VarChar)
        myCommand.Parameters("@lname").Value = TextBox2.Text

        myCommand.Parameters.Add("@tel", OleDbType.VarChar)
        myCommand.Parameters("@tel").Value = TextBox3.Text

        myCommand.Parameters.Add("@email", OleDbType.VarChar)
        myCommand.Parameters("@email").Value = TextBox4.Text

        myCommand.Parameters.Add("@companyid", OleDbType.VarChar)
        myCommand.Parameters("@companyid").Value = DropDownList1.SelectedValue

        myCommand.Parameters.Add("@ip", OleDbType.VarChar)
        myCommand.Parameters("@ip").Value = txtIPAddress.Text

        myCommand.Parameters.Add("@site", OleDbType.VarChar)
        myCommand.Parameters("@site").Value = txtWebsite.Text

        Try
            myCommand.ExecuteNonQuery()
            lbl_success.ForeColor = Drawing.Color.Green
            lbl_success.Text = "Customer successfully altered."
        Catch ex As OleDbException
            lbl_success.ForeColor = Drawing.Color.Red
            lbl_success.Text = "ERROR: Could not alter record, please ensure the fields are correctly filled out"
        End Try

        myCommand.Connection.Close()

    End Sub


End Class
