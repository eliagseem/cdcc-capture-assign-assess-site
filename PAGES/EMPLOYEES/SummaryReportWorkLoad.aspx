﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SummaryReportWorkLoad.aspx.vb" Inherits="PAGES_EMPLOYEES_StatusReport" MaintainScrollPositionOnPostBack="True" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
        <script type = "text/javascript">
            function PrintPanel() {
                var panel = document.getElementById("<%=pnlContents.ClientID %>");
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write('<html><head><title>DIV Contents</title>');
                printWindow.document.write('</head><body >');
                printWindow.document.write(panel.innerHTML);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                setTimeout(function () {
                    printWindow.print();
                }, 500);
                return false;
            }
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:767px;"> 
       <asp:Panel ID="pnlContents" runat="server"> 
        <p>
       <h3>RESOURCES WORK LOAD BY EMPLOYEE/DEPARTMENT REPORT
           <br /><asp:Label ID="Label4" runat="server"></asp:Label>
            </h3>
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="828px" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="446px">
                <LocalReport ReportPath="PAGES\EMPLOYEES\Reports\WorkLoadReport.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="WorkLoadDS" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT In_Progress_Request.InProgReqID, In_Progress_Request.Status, In_Progress_Request.EmployeeID, In_Progress_Request.DepartmentID, In_Progress_Request.EstimatedDeadline, In_Progress_Request.DateAssigned, Employee.EmployeeName, Department.DepartmentName FROM ((In_Progress_Request LEFT OUTER JOIN Department ON In_Progress_Request.DepartmentID = Department.DepartmentID) LEFT OUTER JOIN Employee ON In_Progress_Request.EmployeeID = Employee.EmployeeID) WHERE (In_Progress_Request.Status = 'InProgress')"
                FilterExpression="EmployeeID={0} OR DepartmentID={0}">
                <FilterParameters>
                    <asp:ControlParameter Name="EmployeeID" ControlId="DropDownList1" PropertyName="SelectedValue"/>
                     <asp:ControlParameter Name="DepartmentID" ControlId="DropDownList2" PropertyName="SelectedValue"/>
                </FilterParameters></asp:SqlDataSource>
    </p>
        </asp:Panel>
        Filter by Employee Name:&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="empDDLfill" DataTextField="EmployeeName" DataValueField="EmployeeID" AutoPostBack="True" AppendDataBoundItems="True">
        <asp:ListItem Text="None" Value="0" />  
        </asp:DropDownList>
        <asp:SqlDataSource ID="empDDLfill" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [EmployeeID], [EmployeeName] FROM [Employee] ORDER BY [EmployeeID]"></asp:SqlDataSource>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        &nbsp;<br />Filter by Department Name:
        <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" DataSourceID="depFillDS" DataTextField="DepartmentName" DataValueField="DepartmentID" AppendDataBoundItems="True">
        <asp:ListItem Text="None" Value="0" />  
        </asp:DropDownList>
        <asp:SqlDataSource ID="depFillDS" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Department] ORDER BY [DepartmentID]"></asp:SqlDataSource>
&nbsp;<p>
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/PAGES/EMPLOYEES/SummaryReportWorkLoad.aspx">Show full table and remove filters</asp:HyperLink>
        </p>
    <p>
        &nbsp;</p></section>
</asp:Content>

