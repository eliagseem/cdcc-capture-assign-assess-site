﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditInquiry.aspx.vb" Inherits="PAGES_EMPLOYEES_EditInquiry" MaintainScrollPositionOnPostBack="True"%>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="../../scripts/jquery-ui.min.js"></script>
    <script src="../../scripts/jquery-1.11.2.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script>
        window.onload = toggleAll;

        function toggleAll() {
            $('#hideMe').toggle();
        };

        function toggleCal() {
            $('#hideMe').toggle();
        }
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;">  <p>
        EDIT Inquiry: Select the row that you want to edit<asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="InquiryID" DataSourceID="SqlDataSource1" AllowPaging="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="InquiryID" HeaderText="InquiryID" InsertVisible="False" ReadOnly="True" SortExpression="InquiryID" />
                <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
                <asp:BoundField DataField="EmployeeID" HeaderText="EmployeeID" SortExpression="EmployeeID" />
                <asp:BoundField DataField="CustomerID" HeaderText="CustomerID" SortExpression="CustomerID" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" DeleteCommand="DELETE FROM [Inquiry] WHERE [InquiryID] = ?" InsertCommand="INSERT INTO [Inquiry] ([InquiryID], [DateCreated], [EmployeeID], [CustomerID]) VALUES (?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT * FROM [Inquiry]" UpdateCommand="UPDATE [Inquiry] SET [DateCreated] = ?, [EmployeeID] = ?, [CustomerID] = ? WHERE [InquiryID] = ?" FilterExpression="CONVERT([InquiryID], 'System.String') LIKE '{0}'">
            <DeleteParameters>
                <asp:Parameter Name="InquiryID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="InquiryID" Type="Int32" />
                <asp:Parameter Name="DateCreated" Type="DateTime" />
                <asp:Parameter Name="EmployeeID" Type="Int32" />
                <asp:Parameter Name="CustomerID" Type="Int32" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="DateCreated" Type="DateTime" />
                <asp:Parameter Name="EmployeeID" Type="Int32" />
                <asp:Parameter Name="CustomerID" Type="Int32" />
                <asp:Parameter Name="InquiryID" Type="Int32" />
            </UpdateParameters>
                                        <FilterParameters>
        <asp:ControlParameter Name="InquiryID" ControlID="txt_filter" PropertyName="Text" />
    </FilterParameters>
        </asp:SqlDataSource>
    </p>
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT * FROM [Inquiry_Request]"></asp:SqlDataSource>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="InquiryReqID" DataSourceID="SqlDataSource6" Height="171px" Visible="False" Width="594px">
            <Columns>
                <asp:BoundField DataField="InquiryReqID" HeaderText="InquiryReqID" InsertVisible="False" ReadOnly="True" SortExpression="InquiryReqID" />
                <asp:BoundField DataField="RequestID" HeaderText="RequestID" SortExpression="RequestID" />
                <asp:BoundField DataField="InquiryID" HeaderText="InquiryID" SortExpression="InquiryID" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
    <p>
        Filter by Inquiry ID:
        <asp:TextBox ID="txt_filter" runat="server"></asp:TextBox>
    &nbsp;<asp:Button ID="Button4" runat="server" Text="FILTER" />
    </p>
        <p>
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/PAGES/EMPLOYEES/EditInquiry.aspx">De-select row and show full table</asp:HyperLink>
    </p>
    <h3>
        INQUIRY FORM:</h3>
    <p>
        Date logged:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="TextBox2" runat="server" Width="128px"></asp:TextBox>
    &nbsp;<img style="cursor: pointer;" ID="ImageButton1" onclick="toggleCal();" src="../../IMAGES/calendar_view_day.png" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox2" ErrorMessage="This field is required." ForeColor="Red"></asp:RequiredFieldValidator>
        </p> 
        <p>
            <div id="hideMe"><asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px">
                <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                <TodayDayStyle BackColor="#CCCCCC" />
                </asp:Calendar></div>
    </p>
    <p>
        Recipient employee: <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource2" DataTextField="EmployeeName" DataValueField="EmployeeID">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT EmployeeID, EmployeeName FROM Employee ORDER BY EmployeeID"></asp:SqlDataSource>
    </p>
    <p>
        Customer:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="SqlDataSource3" DataTextField="CustomerFullName" DataValueField="CustomerID">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT CustomerID, CustomerFName &amp; ' ' &amp; CustomerLName AS CustomerFullName FROM Customer ORDER BY CustomerID"></asp:SqlDataSource>
    &nbsp;<asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/PAGES/EMPLOYEES/CreateCustomer.aspx" Target="_blank " Font-Size="Medium">(Click here to create a new Customer)</asp:HyperLink>
    </p>
    <p>
        Topic:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="DropDownList4" runat="server" DataSourceID="SqlDataSource4" DataTextField="Topic" DataValueField="RequestID">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT [RequestID], [Topic] FROM [Request] ORDER BY [RequestID]"></asp:SqlDataSource>
    <br />&nbsp;<asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/PAGES/EMPLOYEES/CreateRequest.aspx" Target="_blank " Font-Size="Medium">(Click here to create a new Request)</asp:HyperLink>
    </p>
        <p>
        <asp:Button ID="Button5" runat="server" Text="Refresh Lists" Width="85px" />
    </p>
    <p>
        <asp:Button ID="Button3" runat="server" Text="SAVE INQUIRY" />
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT * FROM [Inquiry_Request]"></asp:SqlDataSource>
    </p>
        <p>
        <asp:Label ID="lbl_success" runat="server" ForeColor="Green"></asp:Label>
    </p>
    <p>
        &nbsp;</p></section>
</asp:Content>

