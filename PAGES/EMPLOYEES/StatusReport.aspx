﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="StatusReport.aspx.vb" Inherits="PAGES_EMPLOYEES_StatusReport" MaintainScrollPositionOnPostBack="True" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
        <script type = "text/javascript">
            function PrintPanel() {
                var panel = document.getElementById("<%=pnlContents.ClientID %>");
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;"> <p>
       <asp:Panel ID="pnlContents" runat="server">
        <h3>STATUS REPORT</h3>
           <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="628px" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
               <LocalReport ReportPath="PAGES\EMPLOYEES\Reports\StatusReport.rdlc">
                   <DataSources>
                       <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="StatusRepDS" />
                   </DataSources>
               </LocalReport>
           </rsweb:ReportViewer>
           <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT RequestID, Topic, Frequency, DateCreated, Status FROM Request ORDER BY RequestID"
               FilterExpression="Status='{0}'">
                <FilterParameters>
                    <asp:ControlParameter Name="Status" ControlId="RadioButtonList1" PropertyName="SelectedValue"/>
                </FilterParameters>

           </asp:SqlDataSource>
           <asp:ScriptManager ID="ScriptManager1" runat="server">
           </asp:ScriptManager>
        </asp:Panel>
        Filter by Status (Open, InProgress, Resolved):
        <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True">
            <asp:ListItem>Open</asp:ListItem>
            <asp:ListItem>InProgress</asp:ListItem>
            <asp:ListItem>Resolved</asp:ListItem>
        </asp:RadioButtonList>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/PAGES/EMPLOYEES/StatusReport.aspx">Click here to reset the filter</asp:HyperLink>
    <p>
        &nbsp;</p></section>
</asp:Content>

