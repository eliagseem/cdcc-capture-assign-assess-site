﻿Imports System.Data.OleDb

Partial Class PAGES_EMPLOYEES_EditRequest
    Inherits System.Web.UI.Page

    Protected Sub Calendar1_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar1.SelectionChanged
        txtDateCreated.Text = Calendar1.SelectedDate.ToShortDateString
        txtDateCreated.Text += " " + DateTime.Now.ToShortTimeString
    End Sub

    Protected Sub Calendar2_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar2.SelectionChanged
        txtEstDeadline.Text = Calendar2.SelectedDate.ToShortDateString
        txtEstDeadline.Text += " " + DateTime.Now.ToShortTimeString
    End Sub

    Protected Sub Calendar3_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar3.SelectionChanged
        txtDateAssigned.Text = Calendar3.SelectedDate.ToShortDateString
        txtDateAssigned.Text += " " + DateTime.Now.ToShortTimeString
    End Sub

    Protected Sub Calendar4_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar4.SelectionChanged
        txtDateResolved.Text = Calendar4.SelectedDate.ToShortDateString
        txtDateResolved.Text += " " + DateTime.Now.ToShortTimeString
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView1.SelectedIndexChanged
        MultiView1.Visible = True

        If (GridView1.SelectedRow.Cells(6).Text = "Open") Then
            Button5.Visible = False
            GridView2.Visible = False
            GridView3.Visible = False
        ElseIf (GridView1.SelectedRow.Cells(6).Text = "InProgress") Then
            If (HttpContext.Current.User.Identity.IsAuthenticated) Then
                If (User.IsInRole("EMPLOYEE") = True) Then
                    Button5.Visible = False
                    Button11.Visible = False
                    GridView2.Visible = False
                    GridView3.Visible = False
                Else
                    Button5.Visible = True
                    Button11.Visible = False
                    GridView2.Visible = True
                    GridView3.Visible = False
                    Dim rowId As Integer = CInt(GridView1.DataKeys(GridView1.SelectedIndex).Value)
                    InProgressDS.FilterExpression = "RequestID = " & rowId
                    
                End If
        End If
            
        ElseIf (GridView1.SelectedRow.Cells(6).Text = "Resolved") Then

            If (HttpContext.Current.User.Identity.IsAuthenticated) Then
                If (User.IsInRole("EMPLOYEE") = True) Then
                    Button5.Visible = False
                    Button11.Visible = False
                    GridView2.Visible = False
                    GridView3.Visible = False
                Else
                    Button5.Visible = True
                    Button11.Visible = True

                    GridView2.Visible = True
                    Dim rowId As Integer = CInt(GridView1.DataKeys(GridView1.SelectedIndex).Value)
                    InProgressDS.FilterExpression = "RequestID = " & rowId

                    GridView3.Visible = True

                    rowId = CInt(GridView1.DataKeys(GridView1.SelectedIndex).Value)
                    ResolvedDS.FilterExpression = "Resolved_Request.RequestID = " & rowId
                End If
            End If

        End If

        txtDateCreated.Text = GridView1.SelectedRow.Cells(4).Text
        radFreq.SelectedValue = GridView1.SelectedRow.Cells(2).Text
        txtDetailedInfo.Text = GridView1.SelectedRow.Cells(3).Text
        txtTopic.Text = GridView1.SelectedRow.Cells(5).Text
        radStatus.SelectedValue = GridView1.SelectedRow.Cells(6).Text


    End Sub

    Protected Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()

        Dim myCommand As OleDbCommand
        Dim alterCmd As String


        alterCmd = "UPDATE Resolved_Request SET DateResolved = @dateRes, SpecialNotes = @notes, ResolutionSatisfaction = @resSatisfaction WHERE RequestID = " + GridView1.SelectedRow.Cells(1).Text + ";"
        myCommand = New OleDbCommand(alterCmd, conn)

        myCommand.Parameters.Add("@dateRes", OleDbType.Date)
        myCommand.Parameters("@dateRes").Value = txtDateResolved.Text
        myCommand.Parameters.Add("@notes", OleDbType.VarChar)
        myCommand.Parameters("@notes").Value = txtSpecialNotes.Text
        myCommand.Parameters.Add("@resSatisfaction", OleDbType.VarChar)
        myCommand.Parameters("@resSatisfaction").Value = txtResolution.Text



        Try
            myCommand.ExecuteNonQuery()
            lbl_success1.ForeColor = Drawing.Color.Green
            lbl_success1.Text = "Resolved Request successfully altered."
        Catch ex As OleDbException
            lbl_success1.ForeColor = Drawing.Color.Red
            lbl_success1.Text = "ERROR: Could not alter record, please ensure the fields are correctly filled out. If error persists, please return to the home page and come back. "
        End Try
        myCommand.Connection.Close()
        conn.Close()
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If (HttpContext.Current.User.Identity.IsAuthenticated) Then
            If (User.IsInRole("EMPLOYEE") = True) Then
                Button5.Visible = False
                Button11.Visible = False
            End If

        End If


    End Sub

    Protected Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        If IsNothing(GridView1.SelectedRow) Then
            lbl_success.ForeColor = Drawing.Color.Red
            lbl_success.Text = "ERROR: You must select a row/record to edit from the grid before saving!"
        ElseIf (GridView1.SelectedRow.Cells(6).Text = "Open") Then
            If radStatus.SelectedValue = "InProgress" Then
                'create new in progress request record in table and link it to the aforementioned request
                Dim conn2 As OleDbConnection
                conn2 = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
                conn2.Open()

                Dim myCommand2 As OleDbCommand
                Dim insertCmd As String

                insertCmd = "INSERT into In_Progress_Request(Status, EmployeeID, DepartmentID, EstimatedDeadline, DateAssigned, CustomerFeedback, RequestID) values(@status, @empID, @depID, @estDeadline, @dateAss, @custFeed, @reqID);"
                myCommand2 = New OleDbCommand(insertCmd, conn2)

                myCommand2.Parameters.Add("@status", OleDbType.VarChar)
                myCommand2.Parameters("@status").Value = "InProgress"
                myCommand2.Parameters.Add("@empID", OleDbType.VarChar)
                myCommand2.Parameters("@empID").Value = "0"
                myCommand2.Parameters.Add("@depID", OleDbType.VarChar)
                myCommand2.Parameters("@depID").Value = "0"
                myCommand2.Parameters.Add("@estDeadline", OleDbType.Date)
                myCommand2.Parameters("@estDeadline").Value = DateTime.Now.ToShortDateString + " " + DateTime.Now.ToShortTimeString
                myCommand2.Parameters.Add("@dateAss", OleDbType.Date)
                myCommand2.Parameters("@dateAss").Value = DateTime.Now.ToShortDateString + " " + DateTime.Now.ToShortTimeString
                myCommand2.Parameters.Add("@custFeed", OleDbType.VarChar)
                myCommand2.Parameters("@custFeed").Value = "Unsatisfied"
                myCommand2.Parameters.Add("@reqID", OleDbType.VarChar)
                myCommand2.Parameters("@reqID").Value = GridView1.SelectedRow.Cells(1).Text

                Try
                    myCommand2.ExecuteNonQuery()

                Catch ex As OleDbException
                    lbl_success.Text = "ERROR: Could not add record, please ensure the fields are correctly filled out"
                End Try

                myCommand2.Connection.Close()



                'Modify base request record as well
                Dim conn As OleDbConnection
                conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
                conn.Open()

                Dim myCommand As OleDbCommand
                Dim alterCmd As String


                alterCmd = "UPDATE Request SET Details = @details, Topic = @topic, DateCreated = @date, Frequency = @frequency, Status = @status WHERE RequestID = " + GridView1.SelectedRow.Cells(1).Text + ";"
                myCommand = New OleDbCommand(alterCmd, conn)

                myCommand.Parameters.Add("@details", OleDbType.VarChar)
                myCommand.Parameters("@details").Value = txtDetailedInfo.Text

                myCommand.Parameters.Add("@topic", OleDbType.VarChar)
                myCommand.Parameters("@topic").Value = txtTopic.Text

                myCommand.Parameters.Add("@date", OleDbType.Date)
                myCommand.Parameters("@date").Value = txtDateCreated.Text

                myCommand.Parameters.Add("@frequency", OleDbType.VarChar)
                myCommand.Parameters("@frequency").Value = radFreq.SelectedValue

                myCommand.Parameters.Add("@status", OleDbType.VarChar)
                myCommand.Parameters("@status").Value = radStatus.SelectedValue
                Try
                    myCommand.ExecuteNonQuery()
                    lbl_success.ForeColor = Drawing.Color.Green
                    lbl_success.Text = "Request successfully modified. Transition from Open to In-Progress successful!"
                    GridView1.DataBind()

                Catch ex As OleDbException
                    lbl_success.ForeColor = Drawing.Color.Red
                    lbl_success.Text = "ERROR: Could not add record, please ensure the fields are correctly filled out"
                End Try

                myCommand.Connection.Close()


            ElseIf radStatus.SelectedValue = "Resolved" Then
                'throw error
                lbl_success.ForeColor = Drawing.Color.Red
                lbl_success.Text = "ERROR: You cannot go directly from an open request to a resolved one! A request must be in-progress before it is resolved."


            Else
                'Modify base request record only
                Dim conn As OleDbConnection
                conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
                conn.Open()

                Dim myCommand As OleDbCommand
                Dim alterCmd As String


                alterCmd = "UPDATE Request SET Details = @details, Topic = @topic, DateCreated = @date, Frequency = @frequency, Status = @status WHERE RequestID = " + GridView1.SelectedRow.Cells(1).Text + ";"
                myCommand = New OleDbCommand(alterCmd, conn)

                myCommand.Parameters.Add("@details", OleDbType.VarChar)
                myCommand.Parameters("@details").Value = txtDetailedInfo.Text

                myCommand.Parameters.Add("@topic", OleDbType.VarChar)
                myCommand.Parameters("@topic").Value = txtTopic.Text

                myCommand.Parameters.Add("@date", OleDbType.Date)
                myCommand.Parameters("@date").Value = txtDateCreated.Text

                myCommand.Parameters.Add("@frequency", OleDbType.VarChar)
                myCommand.Parameters("@frequency").Value = radFreq.SelectedValue

                myCommand.Parameters.Add("@status", OleDbType.VarChar)
                myCommand.Parameters("@status").Value = radStatus.SelectedValue


                Try
                    myCommand.ExecuteNonQuery()

                    lbl_success.ForeColor = Drawing.Color.Green
                    lbl_success.Text = "Request successfully altered."

                    GridView1.DataBind()
                Catch ex As OleDbException
                    lbl_success.ForeColor = Drawing.Color.Red
                    lbl_success.Text = "ERROR: Could not alter record, please ensure the fields are correctly filled out"
                End Try

                myCommand.Connection.Close()
                conn.Close()

            End If



        ElseIf (GridView1.SelectedRow.Cells(6).Text = "InProgress") Then
            If radStatus.SelectedValue = "Resolved" Then
                'create new resolved request record in table with request foreign key

                Dim conn2 As OleDbConnection
                conn2 = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
                conn2.Open()

                Dim myCommand2 As OleDbCommand
                Dim insertCmd As String

                insertCmd = "INSERT into Resolved_Request(DateResolved, SpecialNotes, ResolutionSatisfaction, RequestID) values(@DateResolved, @specialnotes, @ressatisfaction, @reqID);"
                myCommand2 = New OleDbCommand(insertCmd, conn2)

                myCommand2.Parameters.Add("@DateResolved", OleDbType.Date)
                myCommand2.Parameters("@DateResolved").Value = DateTime.Now.ToShortDateString + " " + DateTime.Now.ToShortTimeString
                myCommand2.Parameters.Add("@specialnotes", OleDbType.VarChar)
                myCommand2.Parameters("@specialnotes").Value = "None."
                myCommand2.Parameters.Add("@ressatisfaction", OleDbType.VarChar)
                myCommand2.Parameters("@ressatisfaction").Value = "Unsatisfied"
                myCommand2.Parameters.Add("@reqID", OleDbType.VarChar)
                myCommand2.Parameters("@reqID").Value = GridView1.SelectedRow.Cells(1).Text

                Try
                    myCommand2.ExecuteNonQuery()

                Catch ex As OleDbException
                    lbl_success.ForeColor = Drawing.Color.Red
                    lbl_success.Text = "ERROR: Could not add record, please ensure the fields are correctly filled out"
                End Try
                myCommand2.Connection.Close()

                'Modify base request record as well
                Dim conn As OleDbConnection
                conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
                conn.Open()

                Dim myCommand As OleDbCommand
                Dim alterCmd As String


                alterCmd = "UPDATE Request SET Details = @details, Topic = @topic, DateCreated = @date, Frequency = @frequency, Status = @status WHERE RequestID = " + GridView1.SelectedRow.Cells(1).Text + ";"
                myCommand = New OleDbCommand(alterCmd, conn)

                myCommand.Parameters.Add("@details", OleDbType.VarChar)
                myCommand.Parameters("@details").Value = txtDetailedInfo.Text

                myCommand.Parameters.Add("@topic", OleDbType.VarChar)
                myCommand.Parameters("@topic").Value = txtTopic.Text

                myCommand.Parameters.Add("@date", OleDbType.Date)
                myCommand.Parameters("@date").Value = txtDateCreated.Text

                myCommand.Parameters.Add("@frequency", OleDbType.VarChar)
                myCommand.Parameters("@frequency").Value = radFreq.SelectedValue

                myCommand.Parameters.Add("@status", OleDbType.VarChar)
                myCommand.Parameters("@status").Value = radStatus.SelectedValue

                Try
                    myCommand.ExecuteNonQuery()
                    lbl_success.ForeColor = Drawing.Color.Green
                    lbl_success.Text = "Request successfully modified. The transition from In-Progress to Resolved was successful!"

                Catch ex As OleDbException
                    lbl_success.ForeColor = Drawing.Color.Red
                    lbl_success.Text = "ERROR: Could not update record, please ensure the fields are correctly filled out"
                End Try
                myCommand.Connection.Close()

                Dim conn3 As OleDbConnection
                conn3 = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
                conn3.Open()

                Dim myCommand3 As OleDbCommand
                Dim alterCmd2 As String


                alterCmd2 = "UPDATE In_Progress_Request SET Status = @status WHERE RequestID = " + GridView1.SelectedRow.Cells(1).Text + ";"
                myCommand3 = New OleDbCommand(alterCmd2, conn3)

                myCommand3.Parameters.Add("@status", OleDbType.VarChar)
                myCommand3.Parameters("@status").Value = radStatus.SelectedValue


                Try
                    myCommand3.ExecuteNonQuery()
                    lbl_successInProg.ForeColor = Drawing.Color.Green
                    lbl_successInProg.Text = "In Progress Request successfully altered."
                Catch ex As OleDbException
                    lbl_successInProg.ForeColor = Drawing.Color.Red
                    lbl_successInProg.Text = "ERROR: Could not alter record, please ensure the fields are correctly filled out. If error persists, please return to the home page and come back. "
                End Try

                myCommand3.Connection.Close()



            ElseIf radStatus.SelectedValue = "Open" Then
                'throw error
                lbl_success.ForeColor = Drawing.Color.Red
                lbl_success.Text = "ERROR: You cannot change a request from being in-progress to open."

            Else

                'Modify base request record only
                Dim conn As OleDbConnection
                conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
                conn.Open()

                Dim myCommand As OleDbCommand
                Dim alterCmd As String


                alterCmd = "UPDATE Request SET Details = @details, Topic = @topic, DateCreated = @date, Frequency = @frequency, Status = @status WHERE RequestID = " + GridView1.SelectedRow.Cells(1).Text + ";"
                myCommand = New OleDbCommand(alterCmd, conn)

                myCommand.Parameters.Add("@details", OleDbType.VarChar)
                myCommand.Parameters("@details").Value = txtDetailedInfo.Text

                myCommand.Parameters.Add("@topic", OleDbType.VarChar)
                myCommand.Parameters("@topic").Value = txtTopic.Text

                myCommand.Parameters.Add("@date", OleDbType.Date)
                myCommand.Parameters("@date").Value = txtDateCreated.Text

                myCommand.Parameters.Add("@frequency", OleDbType.VarChar)
                myCommand.Parameters("@frequency").Value = radFreq.SelectedValue

                myCommand.Parameters.Add("@status", OleDbType.VarChar)
                myCommand.Parameters("@status").Value = radStatus.SelectedValue

                Try
                    myCommand.ExecuteNonQuery()
                    lbl_success.ForeColor = Drawing.Color.Green
                    lbl_success.Text = "Request successfully altered."
                Catch ex As OleDbException
                    lbl_success.ForeColor = Drawing.Color.Red
                    lbl_success.Text = "ERROR: Could not update record, please ensure the fields are correctly filled out"
                End Try
                myCommand.Connection.Close()


            End If

        ElseIf (GridView1.SelectedRow.Cells(6).Text = "Resolved") Then

            If radStatus.SelectedValue = "Open" Then
                'throw error
                lbl_success1.ForeColor = Drawing.Color.Red
                lbl_success.Text = "ERROR: You cannot change a request from being resolved to open. The request must be deleted if it is no longer relevant."
            ElseIf radStatus.SelectedValue = "InProgress" Then
                'throw error
                lbl_success1.ForeColor = Drawing.Color.Red
                lbl_success.Text = "ERROR: You cannot change a request from being resolved to in-progress. The request must be deleted if it is no longer relevant."
            Else
                'Modify base request record only
                Dim conn As OleDbConnection
                conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
                conn.Open()

                Dim myCommand As OleDbCommand
                Dim alterCmd As String


                alterCmd = "UPDATE Request SET Details = @details, Topic = @topic, DateCreated = @date, Frequency = @frequency, Status = @status WHERE RequestID = " + GridView1.SelectedRow.Cells(1).Text + ";"
                myCommand = New OleDbCommand(alterCmd, conn)

                myCommand.Parameters.Add("@details", OleDbType.VarChar)
                myCommand.Parameters("@details").Value = txtDetailedInfo.Text

                myCommand.Parameters.Add("@topic", OleDbType.VarChar)
                myCommand.Parameters("@topic").Value = txtTopic.Text

                myCommand.Parameters.Add("@date", OleDbType.Date)
                myCommand.Parameters("@date").Value = txtDateCreated.Text

                myCommand.Parameters.Add("@frequency", OleDbType.VarChar)
                myCommand.Parameters("@frequency").Value = radFreq.SelectedValue

                myCommand.Parameters.Add("@status", OleDbType.VarChar)
                myCommand.Parameters("@status").Value = radStatus.SelectedValue

                Try
                    myCommand.ExecuteNonQuery()
                    lbl_success.ForeColor = Drawing.Color.Green
                    lbl_success.Text = "Request successfully altered."
                Catch ex As OleDbException
                    lbl_success.ForeColor = Drawing.Color.Green
                    lbl_success.Text = "ERROR: Could not update record, please ensure the fields are correctly filled out"
                End Try
                myCommand.Connection.Close()
            End If


            'end big if
            End If



    End Sub

    Protected Sub GridView2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView2.SelectedIndexChanged

    End Sub

    Protected Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Dim deadline As DateTime = Convert.ToDateTime(txtEstDeadline.Text)
        Dim dateAssigned As DateTime = Convert.ToDateTime(txtDateAssigned.Text)
        Dim todayDate As DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)

        'If deadline < todayDate Then
        '    lbl_successInProg.ForeColor = Drawing.Color.Red
        '    lbl_successInProg.Text = "ERROR: The estimated deadline cannot be before today's date! "

        'ElseIf dateAssigned > deadline Then
        '    lbl_successInProg.ForeColor = Drawing.Color.Red
        '    lbl_successInProg.Text = "ERROR: A request cannot be assigned after the estimated deadline!"

        'Else


        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()

        Dim myCommand As OleDbCommand
        Dim alterCmd As String


        alterCmd = "UPDATE In_Progress_Request SET EmployeeID = @empID, DepartmentID = @depID, EstimatedDeadline = @estdeadline, DateAssigned = @dateAss, CustomerFeedback = @custfeedback WHERE RequestID = " + GridView1.SelectedRow.Cells(1).Text + ";"
        myCommand = New OleDbCommand(alterCmd, conn)

        myCommand.Parameters.Add("@empID", OleDbType.VarChar)
        myCommand.Parameters("@empID").Value = dListEmp.SelectedValue

        myCommand.Parameters.Add("@depID", OleDbType.VarChar)
        myCommand.Parameters("@depID").Value = dListDep.SelectedValue

        myCommand.Parameters.Add("@estdeadline", OleDbType.Date)
        myCommand.Parameters("@estdeadline").Value = txtEstDeadline.Text

        myCommand.Parameters.Add("@dateAss", OleDbType.Date)
        myCommand.Parameters("@dateAss").Value = txtDateAssigned.Text

        myCommand.Parameters.Add("@custfeedback", OleDbType.VarChar)
        myCommand.Parameters("@custfeedback").Value = radSatisfaction.SelectedValue



        Try
            myCommand.ExecuteNonQuery()
            lbl_successInProg.ForeColor = Drawing.Color.Green
            lbl_successInProg.Text = "In Progress Request successfully altered."
        Catch ex As OleDbException
            lbl_successInProg.ForeColor = Drawing.Color.Red
            lbl_successInProg.Text = "ERROR: Could not alter record, please ensure the fields are correctly filled out. If error persists, please return to the home page and come back. "
        End Try

        myCommand.Connection.Close()
        'End If

    End Sub

    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        dListEmp.Items.Clear()
        dListEmp.Items.Insert(0, New ListItem("None", "0"))
        dListEmp.AppendDataBoundItems = True
        dListEmp.DataBind()


        dListDep.Items.Clear()
        dListDep.Items.Insert(0, New ListItem("None", "0"))
        dListDep.AppendDataBoundItems = True
        dListDep.DataBind()


        dListEmp.SelectedValue = GridView2.Rows(0).Cells(2).Text
        dListDep.SelectedValue = GridView2.Rows(0).Cells(3).Text


        txtEstDeadline.Text = GridView2.Rows(0).Cells(4).Text
        txtDateAssigned.Text = GridView2.Rows(0).Cells(5).Text
        radSatisfaction.SelectedValue = GridView2.Rows(0).Cells(6).Text

    End Sub


    Protected Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        txtDateResolved.Text = GridView3.Rows(0).Cells(1).Text
        txtResolution.Text = GridView3.Rows(0).Cells(4).Text
        txtSpecialNotes.Text = GridView3.Rows(0).Cells(3).Text
    End Sub

    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

    End Sub

    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        dListEmp.Items.Clear()
        dListEmp.Items.Insert(0, New ListItem("None", "0"))
        dListEmp.AppendDataBoundItems = True
        dListEmp.DataBind()


        dListDep.Items.Clear()
        dListDep.Items.Insert(0, New ListItem("None", "0"))
        dListDep.AppendDataBoundItems = True
        dListDep.DataBind()



        dListEmp.SelectedValue = GridView2.Rows(0).Cells(2).Text
        dListDep.SelectedValue = GridView2.Rows(0).Cells(3).Text


        txtEstDeadline.Text = GridView2.Rows(0).Cells(4).Text
        txtDateAssigned.Text = GridView2.Rows(0).Cells(5).Text
        radSatisfaction.SelectedValue = GridView2.Rows(0).Cells(6).Text


    End Sub

    Protected Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click

    End Sub
End Class
