﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DepCustCompManagement.aspx.vb" Inherits="PAGES_EMPLOYEES_DepCustCompManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:300px;padding-bottom:24px;padding-left:50px;height:490px; margin-right:5px;margin-left:5px;width:1114px;"><p>
    <span style="font-size:1.2em;"><strong>DEPARTMENT, CUSTOMER &amp; EXTERNAL ORGANIZATION MANAGEMENT</strong></span></p>
    <p style="margin-left:40px;margin-top:30px;">
			<asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/IMAGES/buttons/createCompany.png" />
         &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/IMAGES/buttons/createCustomer.png" />
         &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/IMAGES/buttons/createDepartment.png" />
         &nbsp;&nbsp;&nbsp;&nbsp;
        <br /><br />
			<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/IMAGES/buttons/editCompany.png" />
        &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/IMAGES/buttons/editCust.png" />
        &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/IMAGES/buttons/editDep.png" />
<br /><br />
            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/IMAGES/buttons/delCompany.png" />
        &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/IMAGES/buttons/delCustomer.png" />
        &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/IMAGES/buttons/delDep.png" />
            </p>
    <p style="margin-left:40px;margin-top:30px;">
			&nbsp;</p></section>
</asp:Content>

