﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ReportManagement.aspx.vb" Inherits="PAGES_EMPLOYEES_ReportManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:700px;">   <p>
			<span style="font-size:1.2em;text-align:center;"><strong>REPORT MANAGEMENT</strong></span></p>
    <p>
			<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/IMAGES/buttons/transReports.png" NavigateUrl="TransactionalReport.aspx"/>
        &nbsp;&nbsp;
        &nbsp;<asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/IMAGES/buttons/statusReports.png" NavigateUrl="StatusReport.aspx"/>
        &nbsp;&nbsp;&nbsp;
        &nbsp;<asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/IMAGES/buttons/summaryReports.png" NavigateUrl="SummaryReports.aspx"/>
        </p>
    <p>
			&nbsp;</p></section>
</asp:Content>

