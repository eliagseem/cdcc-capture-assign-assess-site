﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditRequest.aspx.vb" Inherits="PAGES_EMPLOYEES_EditRequest" MaintainScrollPositionOnPostBack="True" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="../../scripts/jquery-ui.min.js"></script>
    <script src="../../scripts/jquery-1.11.2.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script>
    window.onload = toggleAll;

    function toggleAll() {
        $('#hideMe').toggle();
        $('#hideMe2').toggle();
        $('#hideMe3').toggle();
        $('#hideMe4').toggle();
    };

    function toggleCal() {
        $('#hideMe').toggle();
    }
    function toggleCal2() {
        $('#hideMe2').toggle();
    }
    function toggleCal3() {
        $('#hideMe3').toggle();
    }
    function toggleCal4() {
        $('#hideMe4').toggle();
    }
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;"> <p>
        EDIT Request: Select the row that you want to edit<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="RequestID" DataSourceID="SqlDataSource1" AllowPaging="True" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" PageSize="4" Width="935px">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="RequestID" HeaderText="RequestID" InsertVisible="False" ReadOnly="True" SortExpression="RequestID" />
                <asp:BoundField DataField="Frequency" HeaderText="Frequency" SortExpression="Frequency" />
                <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
                <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
                <asp:BoundField DataField="Topic" HeaderText="Topic" SortExpression="Topic" />
                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" Font-Size="X-Small" ForeColor="White" Wrap="True" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
  
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Request] ORDER BY [RequestID]" FilterExpression="CONVERT([RequestID], 'System.String') LIKE '{0}'">
                                                    <FilterParameters>
        <asp:ControlParameter Name="RequestID" ControlID="txt_filter" PropertyName="Text" />
    </FilterParameters>
        </asp:SqlDataSource>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="InProgReqID" DataSourceID="InProgressDS" Height="171px" Visible="False" Width="934px">
            <Columns>
                <asp:BoundField DataField="InProgReqID" HeaderText="InProgress Request ID" InsertVisible="False" ReadOnly="True" SortExpression="InProgReqID" />
                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                <asp:BoundField DataField="EmployeeID" HeaderText="Assigned Employee (ID)" SortExpression="EmployeeID" />
                <asp:BoundField DataField="DepartmentID" HeaderText="Assigned Department (ID)" SortExpression="DepartmentID" />
                <asp:BoundField DataField="EstimatedDeadline" HeaderText="Estimated Deadline" SortExpression="EstimatedDeadline" />
                <asp:BoundField DataField="DateAssigned" HeaderText="Date Assigned" SortExpression="DateAssigned" />
                <asp:BoundField DataField="CustomerFeedback" HeaderText="Customer Feedback" SortExpression="CustomerFeedback" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="InProgressDS" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT [InProgReqID], [Status], [EmployeeID], [DepartmentID], [EstimatedDeadline], [DateAssigned], [CustomerFeedback], [RequestID] FROM [In_Progress_Request]"></asp:SqlDataSource>
        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="ResolvedDS" Visible="False" Width="936px">
            <Columns>
                <asp:BoundField DataField="ResolvedReqID" HeaderText="ResolvedReqID" InsertVisible="False" SortExpression="ResolvedReqID" />
                <asp:BoundField DataField="DateResolved" HeaderText="DateResolved" SortExpression="DateResolved" />
                <asp:BoundField DataField="Time Spent Resolving" HeaderText="Request Completion Time" SortExpression="Time Spent Resolving" ReadOnly="True" />
                <asp:BoundField DataField="SpecialNotes" HeaderText="SpecialNotes" SortExpression="SpecialNotes" />
                <asp:BoundField DataField="ResolutionSatisfaction" HeaderText="ResolutionSatisfaction" SortExpression="ResolutionSatisfaction" />
                <asp:BoundField DataField="DateAssigned" HeaderText="DateAssigned" SortExpression="DateAssigned" Visible="False" />
                <asp:BoundField DataField="Resolved_Request.RequestID" HeaderText="Resolved_Request.RequestID" SortExpression="Resolved_Request.RequestID" Visible="False" />
                <asp:BoundField DataField="Request.RequestID" HeaderText="Request.RequestID" InsertVisible="False" SortExpression="Request.RequestID" Visible="False" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="ResolvedDS" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT Resolved_Request.ResolvedReqID, Resolved_Request.DateResolved, Fix(((DATEDIFF('n', In_Progress_Request.DateAssigned, Resolved_Request.DateResolved) / 60 / 24))) &amp; ' days, ' &amp; ((DATEDIFF('n', In_Progress_Request.DateAssigned, Resolved_Request.DateResolved) / 60) Mod 24) &amp; ' hours and ' &amp; (DATEDIFF('n', In_Progress_Request.DateAssigned, Resolved_Request.DateResolved) Mod 60) &amp; ' minutes' AS [Time Spent Resolving], Resolved_Request.SpecialNotes, Resolved_Request.ResolutionSatisfaction, In_Progress_Request.DateAssigned, Resolved_Request.RequestID, Request.RequestID FROM ((Resolved_Request INNER JOIN Request ON Resolved_Request.RequestID = Request.RequestID) INNER JOIN In_Progress_Request ON Request.RequestID = In_Progress_Request.RequestID)"></asp:SqlDataSource>
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/PAGES/EMPLOYEES/EditRequest.aspx">De-select row and show full table</asp:HyperLink>
    </p>
    <p>
        Filter by Request ID:&nbsp;
        <asp:TextBox ID="txt_filter" runat="server"></asp:TextBox>
&nbsp;<asp:Button ID="Button4" runat="server" Text="FILTER" />
    </p>
       
           <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" Visible="False">
               <asp:View ID="View1" runat="server">
                       <p><h3>
        WORK REQUEST FORM:</h3>
     <p style="margin-left:50px;">
         Date and time created:&nbsp;&nbsp;

                               <asp:TextBox ID="txtDateCreated" runat="server" Width="169px"></asp:TextBox>

                               &nbsp;<img style="cursor: pointer;" ID="ImageButton1" onclick="toggleCal();" src="../../IMAGES/calendar_view_day.png" />

                               <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDateCreated" ErrorMessage="This field is required." ForeColor="Red"></asp:RequiredFieldValidator>

    <br />
                               Format: DD/MM/YYYY HR:MN AM/PM<br />
                               <div id="hideMe">
                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Calendar ID="Calendar1" runat="server" style="margin-left:40px;" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px">
                                       <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                                       <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                                       <OtherMonthDayStyle ForeColor="#999999" />
                                       <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                                       <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                                       <TodayDayStyle BackColor="#CCCCCC" />
                                   </asp:Calendar>
                               </div>
                               <p style="margin-left:50px;">
                                   &nbsp;Topic:
                                   <asp:TextBox ID="txtTopic" runat="server" Width="270px"></asp:TextBox>
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTopic" ErrorMessage="This field is required." ForeColor="Red"></asp:RequiredFieldValidator>
                               </p>
                               <p style="margin-left:50px;">
                                   <br />
                                   Detailed info:
                                   <br />
                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <asp:TextBox ID="txtDetailedInfo" runat="server" Height="108px" style="margin-top:-15px;" TextMode="MultiLine" Width="182px"></asp:TextBox>
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDetailedInfo" ErrorMessage="This field is required." ForeColor="Red"></asp:RequiredFieldValidator>
                               </p>
                               <p style="margin-left: 40px">
                                   &nbsp;&nbsp; Frequency:&nbsp;
                                   <asp:RadioButtonList ID="radFreq" runat="server" style="margin-left:70px;">
                                       <asp:ListItem>Frequent</asp:ListItem>
                                       <asp:ListItem>Periodical</asp:ListItem>
                                       <asp:ListItem>Rare</asp:ListItem>
                                   </asp:RadioButtonList>
                               </p>
                               <p style="margin-left: 40px">
                                   &nbsp;&nbsp; Status:<asp:RadioButtonList ID="radStatus" runat="server" style="margin-left:70px;">
                                       <asp:ListItem>Open</asp:ListItem>
                                       <asp:ListItem Value="InProgress">In-Progress</asp:ListItem>
                                       <asp:ListItem>Resolved</asp:ListItem>
                                   </asp:RadioButtonList>
                               </p>
                               <p style="margin-left: 40px">
                                   <asp:Button ID="Button10" runat="server" Text="Save Request" />
                                   <asp:Button ID="Button5" runat="server" CommandName="NextView" Text="Status Specific Fields &gt;&gt;"  />
                               </p>
                               <p style="margin-left: 40px">
                                   <asp:Label ID="lbl_success" runat="server" ForeColor="#006600"></asp:Label>
                               </p>
                           <p>
         </p>
                           <p>
         </p>
                           <p>
         </p>
                           </p>
               </asp:View>
                          
               <asp:View ID="View2" runat="server">
                   <h3>
        STATUS-SPECIFIC FIELDS: In-Progress Request Info</h3>
    <p style="margin-left: 40px">
        &nbsp;&nbsp;&nbsp;
        Employee Assigned:&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="dListEmp" runat="server" DataSourceID="empAssignedDS" DataTextField="EmployeeName" DataValueField="EmployeeID">
        </asp:DropDownList>
        <asp:SqlDataSource ID="empAssignedDS" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT [EmployeeID], [EmployeeName] FROM [Employee] ORDER BY [EmployeeID]"></asp:SqlDataSource>
    </p>
    <p style="margin-left: 40px">
        &nbsp;&nbsp;&nbsp;
        Department Assigned:
        <asp:DropDownList ID="dListDep" runat="server" DataSourceID="depAssignedDS" DataTextField="DepartmentName" DataValueField="DepartmentID">
        </asp:DropDownList>
        <asp:SqlDataSource ID="depAssignedDS" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString3 %>" ProviderName="<%$ ConnectionStrings:ConnectionString3.ProviderName %>" SelectCommand="SELECT * FROM [Department] ORDER BY [DepartmentID]"></asp:SqlDataSource>
    </p>
    <p style="margin-left: 40px">
        &nbsp;&nbsp;&nbsp; Estimated deadline:&nbsp;&nbsp;<asp:TextBox ID="txtEstDeadline" runat="server" Width="157px"></asp:TextBox>
        &nbsp;&nbsp;<img style="cursor: pointer;" ID="Img1" onclick="toggleCal2();" src="../../IMAGES/calendar_view_day.png" /><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEstDeadline" ErrorMessage="This field is required." ForeColor="Red"></asp:RequiredFieldValidator>
            <br />
                               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Format: DD/MM/YYYY HR:MN AM/PM<br /><div id="hideMe2"><asp:Calendar ID="Calendar2" runat="server" style="margin-left:20px;" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px">
            <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
            <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
            <OtherMonthDayStyle ForeColor="#999999" />
            <SelectedDayStyle BackColor="#333399" ForeColor="White" />
            <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
            <TodayDayStyle BackColor="#CCCCCC" />
            </asp:Calendar></div>
                   <p style="margin-left: 40px">
                       &nbsp;&nbsp;&nbsp; Date assigned:&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtDateAssigned" runat="server"></asp:TextBox>
                       <img ID="Img2" onclick="toggleCal3();" src="../../IMAGES/calendar_view_day.png" style="cursor: pointer;" />
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDateAssigned" ErrorMessage="This field is required."></asp:RequiredFieldValidator>
                       <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Format: DD/MM/YYYY HR:MN AM/PM<br />
                       <div id="hideMe3">
                           <asp:Calendar ID="Calendar3" runat="server" style="margin-left:20px;" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px">
                               <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                               <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                               <OtherMonthDayStyle ForeColor="#999999" />
                               <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                               <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                               <TodayDayStyle BackColor="#CCCCCC" />
                           </asp:Calendar>
                       </div>
                       <p style="margin-left: 40px">
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       </p>
                       <p style="margin-left: 40px">
                           &nbsp;&nbsp;&nbsp; Customer Feedback:
                           <asp:RadioButtonList ID="radSatisfaction" runat="server" style="margin-left:70px;">
                               <asp:ListItem>Satisfied</asp:ListItem>
                               <asp:ListItem>Unsatisfied</asp:ListItem>
                           </asp:RadioButtonList>
                       </p>
                       <p style="margin-left: 40px">
                           <asp:Button ID="Button6" runat="server" CommandName="PrevView" Text="&lt;&lt; Edit Request Fields" />
                           <asp:Button ID="Button7" runat="server" Text="Save Request" />
                           <asp:Button ID="Button11" runat="server" CommandName="NextView" Text="Edit Resolved Request Fields &gt;&gt;" />
                       </p>
                       <p style="margin-left: 40px">
                           <asp:Label ID="lbl_successInProg" runat="server" ForeColor="#006600"></asp:Label>
                       </p>
                       <p>
                       </p>
                       <p>
                       </p>
                       <p>
                       </p>
                       <p>
                       </p>
                       <p>
                       </p>
        </p>
                   </p>
               </asp:View>
               <asp:View ID="View3" runat="server">
                       <h3>
        STATUS-SPECIFIC FIELDS: Resolved Request Info</h3>
    <p style="margin-left: 40px">

        &nbsp;&nbsp;&nbsp;&nbsp; Date resolved:&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtDateResolved" runat="server" Width="185px"></asp:TextBox>
        <img style="cursor: pointer;" ID="Img3" onclick="toggleCal4();" src="../../IMAGES/calendar_view_day.png" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDateResolved" ErrorMessage="This field is required." ForeColor="Red"></asp:RequiredFieldValidator>
        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Format: DD/MM/YYYY HR:MN AM/PM<br />
        <div id="hideMe4"><asp:Calendar ID="Calendar4" runat="server" style="margin-left:20px;" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px">
            <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
            <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
            <OtherMonthDayStyle ForeColor="#999999" />
            <SelectedDayStyle BackColor="#333399" ForeColor="White" />
            <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
            <TodayDayStyle BackColor="#CCCCCC" />
            </asp:Calendar></div>
        <p style="margin-left: 40px">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </p>
        <p style="margin-left: 40px">
            &nbsp;&nbsp;&nbsp;Resolution Satisfaction:
            <asp:TextBox ID="txtResolution" runat="server" Width="205px"></asp:TextBox>
            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtResolution" ErrorMessage="This field is required." ForeColor="Red"></asp:RequiredFieldValidator>
        </p>
        <p style="margin-left: 40px">
            &nbsp;&nbsp; Special Notes:&nbsp; &nbsp;&nbsp;
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;<asp:TextBox ID="txtSpecialNotes" runat="server" Height="64px" style="margin-top:-15px;" TextMode="MultiLine" Width="127px"></asp:TextBox>
            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtSpecialNotes" ErrorMessage="This field is required." ForeColor="Red"></asp:RequiredFieldValidator>
        </p>
        <p>
            <asp:Button ID="Button8" runat="server" CommandName="PrevView" Text="&lt;&lt; Edit In-Progress Request Fields" />
            <asp:Button ID="Button9" runat="server" Text="Save Request" />
        </p>
        <p>
            <asp:Label ID="lbl_success1" runat="server" ForeColor="#006600"></asp:Label>
        </p>

        <p>
        </p>

        <p>
        </p>

    </p>

               </asp:View>
           </asp:MultiView>


    </section>
</asp:Content>

