﻿Imports System.Data.OleDb
Imports System.Web.Configuration

Partial Class PAGES_EMPLOYEES_CreateDepartment
    Inherits System.Web.UI.Page

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()

        Dim myCommand As OleDbCommand
        Dim insertCmd As String

        If (TextBox1.Text = "") Then
            Exit Sub
        End If

        insertCmd = "INSERT into Department(DepartmentName) values(@dep);"
        myCommand = New OleDbCommand(insertCmd, conn)

        myCommand.Parameters.Add("@dep", OleDbType.VarChar)
        myCommand.Parameters("@dep").Value = TextBox1.Text


        Try
            myCommand.ExecuteNonQuery()
            lbl_success.Text = "Department successfully created!"
        Catch ex As OleDbException
            lbl_success.Text = "ERROR: Could not add record, please ensure the fields are correctly filled out"
        End Try

        myCommand.Connection.Close()
    End Sub
End Class
