﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CreateDepartment.aspx.vb" Inherits="PAGES_EMPLOYEES_CreateDepartment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <h2 style="text-align:center;">
        CREATE DEPARTMENT</h2>
     <p style="margin-left:50px;">
        Department Name:
        <asp:TextBox ID="TextBox1" runat="server" Width="128px"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="Field cannot be blank"></asp:RequiredFieldValidator>
<br />
         <br />
        <asp:Button ID="Button2" runat="server" Text="Create Department" />
    </p>
    <p style="margin-left:50px;">
        <asp:Label ID="lbl_success" runat="server"></asp:Label>
    </p>
    <p style="margin-left:50px;">
        &nbsp;</p>
</asp:Content>

