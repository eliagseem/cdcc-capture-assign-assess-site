﻿<%@ Page Title="CDCC CAA System" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CreateCompany.aspx.vb" Inherits="PAGES_EMPLOYEES_CreateCompany" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <h2 style="text-align:center;">
        CREATE EXTERNAL ORGANIZATION</h2>
     <p style="margin-left:50px;">
        Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txt_name" runat="server" Width="130px"></asp:TextBox>
         &nbsp;
         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_name" ErrorMessage="Field Cannot be left blank!" ForeColor="Red"></asp:RequiredFieldValidator>
         <br />
        Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txt_address" runat="server" Width="135px"></asp:TextBox>
 &nbsp;
         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_address" ErrorMessage="Field Cannot be left blank!" ForeColor="Red"></asp:RequiredFieldValidator>
 <br />
        Telephone:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txt_phone" runat="server" Width="133px"></asp:TextBox>
         &nbsp;
         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_phone" ErrorMessage="Field Cannot be left blank!" ForeColor="Red"></asp:RequiredFieldValidator>
         <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_phone" ErrorMessage="Invalid phone number entered" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" ForeColor="Red"></asp:RegularExpressionValidator>
<br />
         <strong style="color:black">Format:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 1234567891 | [Area code without seperator][Local Number without seperator]</strong><br />
        Industry/Domain:
        <asp:TextBox ID="txt_industry" runat="server" Width="136px"></asp:TextBox>
    &nbsp;
         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txt_industry" ErrorMessage="Field Cannot be left blank!" ForeColor="Red"></asp:RequiredFieldValidator>
    </p>
    <p style="margin-left:50px;">
        <asp:Button ID="Button2" runat="server" Text="Create External Organization" />
    </p>
    <p style="margin-left:50px;">
        <asp:Label ID="lbl_success" runat="server" ForeColor="#009933"></asp:Label>
    </p>
</asp:Content>

