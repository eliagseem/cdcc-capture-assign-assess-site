﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CreateRequest.aspx.vb" Inherits="PAGES_EMPLOYEES_CreateRequest" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="../../scripts/jquery-ui.min.js"></script>
    <script src="../../scripts/jquery-1.11.2.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script>
        window.onload = toggleAll;

        function toggleAll() {
            $('#hideMe').toggle();
        };

        function toggleCal() {
            $('#hideMe').toggle();
        }
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <h2 style="text-align:center;">
        CREATE WORK REQUEST</h2>
     <section style="margin-left:50px;">
         <p>Date created:
         <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
         <img ID="ImageButton1" onclick="toggleCal()" src="../../IMAGES/calendar_view_day.png" />
        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox1" ErrorMessage="Field cannot be blank" ForeColor="Red"></asp:RequiredFieldValidator>
             <div id="hideMe"><asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px">
                 <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                 <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                 <OtherMonthDayStyle ForeColor="#999999" />
                 <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                 <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                 <TodayDayStyle BackColor="#CCCCCC" />
                 </asp:Calendar></div>
&nbsp;<br />
        Topic:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txt_topic" runat="server"></asp:TextBox>

&nbsp;
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_topic" ErrorMessage="Field cannot be blank" ForeColor="Red"></asp:RequiredFieldValidator>

<br />
        Detailed info:
<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txt_details" runat="server" Height="108px" Width="126px" style="margin-top:-15px;" TextMode="multiline"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_details" ErrorMessage="Field cannot be blank" ForeColor="Red"></asp:RequiredFieldValidator>
    </p>
         <p style="margin-left:50px;">
             <asp:Label ID="lbl_success" runat="server" ForeColor="Green"></asp:Label>
<br /><br />
        <asp:Button ID="Button3" runat="server" Text="CREATE REQUEST" />
    </p></section>
    <p style="margin-left:50px;">
        &nbsp;</p>
</asp:Content>

