﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditCustomer.aspx.vb" Inherits="PAGES_EMPLOYEES_EditCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;"> <p>
        EDIT CUSTOMER: Select the row that you want to edit<asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowPaging="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="862px" DataKeyNames="CustomerID">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="CustomerID" HeaderText="Customer ID" InsertVisible="False" SortExpression="CustomerID" />
                <asp:BoundField DataField="CustomerFName" HeaderText="First Name" SortExpression="CustomerFName" />
                <asp:BoundField DataField="CustomerLName" HeaderText="Last Name" SortExpression="CustomerLName" />
                <asp:BoundField DataField="CustomerTelephone" HeaderText="Telephone" SortExpression="CustomerTelephone" />
                <asp:BoundField DataField="CustomerEmail" HeaderText="Email" SortExpression="CustomerEmail" />
                <asp:BoundField DataField="CustomerIPAddress" HeaderText="IP Address" SortExpression="CustomerIPAddress" />
                <asp:BoundField DataField="CustomerWebsite" HeaderText="Website" SortExpression="CustomerWebsite" />
                <asp:BoundField DataField="CompanyName" HeaderText="Related Organization" SortExpression="CompanyName" />
                <asp:BoundField DataField="CompanyID" HeaderText="OrgID" SortExpression="CompanyID" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" DeleteCommand="DELETE FROM [Customer] WHERE [CustomerID] = ?" InsertCommand="INSERT INTO [Customer] ([CustomerID], [CustomerFName], [CustomerLName], [CustomerTelephone], [CustomerEmail], [CompanyID]) VALUES (?, ?, ?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT Customer.CustomerID, Customer.CustomerFName, Customer.CustomerLName, Customer.CustomerTelephone, Customer.CustomerEmail, Customer.CustomerIPAddress, Customer.CustomerWebsite, Customer.CompanyID, Company.CompanyName FROM (Customer INNER JOIN Company ON Customer.CompanyID = Company.CompanyID) ORDER BY Customer.CustomerID" UpdateCommand="UPDATE [Customer] SET [CustomerFName] = ?, [CustomerLName] = ?, [CustomerTelephone] = ?, [CustomerEmail] = ?, [CompanyID] = ? WHERE [CustomerID] = ?" FilterExpression="CONVERT([CustomerID], 'System.String') LIKE '{0}'">
            <DeleteParameters>
                <asp:Parameter Name="CustomerID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="CustomerID" Type="Int32" />
                <asp:Parameter Name="CustomerFName" Type="String" />
                <asp:Parameter Name="CustomerLName" Type="String" />
                <asp:Parameter Name="CustomerTelephone" Type="String" />
                <asp:Parameter Name="CustomerEmail" Type="String" />
                <asp:Parameter Name="CompanyID" Type="Int32" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="CustomerFName" Type="String" />
                <asp:Parameter Name="CustomerLName" Type="String" />
                <asp:Parameter Name="CustomerTelephone" Type="String" />
                <asp:Parameter Name="CustomerEmail" Type="String" />
                <asp:Parameter Name="CompanyID" Type="Int32" />
                <asp:Parameter Name="CustomerID" Type="Int32" />
            </UpdateParameters>
        <FilterParameters>
        <asp:ControlParameter Name="CustomerID" ControlID="txt_filter" PropertyName="Text" />
    </FilterParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        Filter by Customer ID:
        <asp:TextBox ID="txt_filter" runat="server"></asp:TextBox>
    &nbsp;<asp:Button ID="Button4" runat="server" Text="FILTER" CausesValidation="False" />
    </p>
    <h3>
        CUSTOMER FORM:</h3>
    <p>
        First Name:
        <asp:TextBox ID="TextBox5" runat="server" Width="128px"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox5" ErrorMessage="Field cannot be left blank" ForeColor="Red"></asp:RequiredFieldValidator>
    </p>
        <p>
            Last Name:
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox2" ErrorMessage="Field cannot be left blank" ForeColor="Red"></asp:RequiredFieldValidator>
    </p>
    <p>
        Telephone:&nbsp;
        <asp:TextBox ID="TextBox3" runat="server" Width="128px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox3" ErrorMessage="Field cannot be left blank" ForeColor="Red"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox3" ErrorMessage="Invalid phone number" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" ForeColor="Red"></asp:RegularExpressionValidator>
    </p>
    <p>
        Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="TextBox4" runat="server" Width="128px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox4" ErrorMessage="Field cannot be left blank" ForeColor="Red"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox4" ErrorMessage="Invalid email entered" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"></asp:RegularExpressionValidator>
    </p>
        <p>
         IP Address:&nbsp;
        <asp:TextBox ID="txtIPAddress" runat="server" Width="128px"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtIPAddress" ErrorMessage="Field Cannot be left blank!" ForeColor="#FF3300"></asp:RequiredFieldValidator>
&nbsp;
         </p>
        <p>

         Website:&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
        <asp:TextBox ID="txtWebsite" runat="server" Width="128px"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtWebsite" ErrorMessage="Field Cannot be left blank!" ForeColor="#FF3300"></asp:RequiredFieldValidator>
&nbsp;
         </p>
    <p>
        <span style="font-size:.9em">Organization:</span>
        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource2" DataTextField="CompanyName" DataValueField="CompanyID">
        </asp:DropDownList>
&nbsp;
        <asp:HyperLink ID="HyperLink2" runat="server" Font-Size="Medium">(Click here to create a new organization)</asp:HyperLink>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT [CompanyName], [CompanyID] FROM [Company]">
        </asp:SqlDataSource>
    </p>
    <p>
        <asp:Button ID="Button2" runat="server" Text="Save Customer" />
    </p>
    <p>
        <asp:Label ID="lbl_success" runat="server" ForeColor="#009933"></asp:Label>
        </p></section>
</asp:Content>

