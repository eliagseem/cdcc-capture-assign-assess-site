﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CreateInquiry.aspx.vb" Inherits="PAGES_EMPLOYEES_CreateInquiry" MaintainScrollPositionOnPostBack="True" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="../../scripts/jquery-ui.min.js"></script>
    <script src="../../scripts/jquery-1.11.2.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script>
        window.onload = toggleAll;

        function toggleAll() {
            $('#hideMe').toggle();
        }

        function toggleCal() {
            $('#hideMe').toggle();
        }
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <h2 style="text-align:center;">
        CAPTURE AN INQUIRY</h2>
     <p style="margin-left:50px;">
        Date logged:&nbsp;&nbsp;<asp:TextBox ID="TextBox1" runat="server" style="width: 128px"></asp:TextBox>
         <img ID="ImageButton1" onclick="toggleCal()" src="../../IMAGES/calendar_view_day.png" />
    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="Field cannot be blank" Enabled="False" ForeColor="Red"></asp:RequiredFieldValidator>
    </p>
    <p style="margin-left:50px;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br /><div id="hideMe"><asp:Calendar ID="Calendar1" runat="server" style="margin-left:60px;" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px">
        <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
        <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
        <OtherMonthDayStyle ForeColor="#999999" />
        <SelectedDayStyle BackColor="#333399" ForeColor="White" />
        <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
        <TodayDayStyle BackColor="#CCCCCC" />
        </asp:Calendar></div>
    </p>
    <p style="margin-left:50px;">
        <br />
        Recipient employee:&nbsp;&nbsp;&nbsp; <asp:DropDownList ID="ddl_employee" runat="server" DataSourceID="SqlDataSource3" DataTextField="EmployeeName" DataValueField="EmployeeID">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [EmployeeID], [EmployeeName] FROM [Employee] ORDER BY [EmployeeID]"></asp:SqlDataSource>
<br />
        Customer:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:DropDownList ID="ddl_customer" runat="server" DataSourceID="SqlDataSource2" DataTextField="CustomerFullName" DataValueField="CustomerID">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT CustomerID, CustomerFName &amp; ' ' &amp; CustomerLName AS CustomerFullName FROM Customer ORDER BY CustomerID"></asp:SqlDataSource>
&nbsp;<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/PAGES/EMPLOYEES/CreateCustomer.aspx" Target="_blank " Font-Size="Medium">(Click here to create a new Customer)</asp:HyperLink>
<br />
        Topic:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="ddl_topic" runat="server" DataSourceID="SqlDataSource1" DataTextField="Topic" DataValueField="RequestID">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Topic], [RequestID] FROM [Request]"></asp:SqlDataSource>
&nbsp;<asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/PAGES/EMPLOYEES/CreateRequest.aspx" Target="_blank " Font-Size="Medium">(Click here to create a new Request)</asp:HyperLink>
    </p>
    <p style="margin-left:50px;">
        <asp:Button ID="Button4" runat="server" Text="Refresh Lists" Width="85px" />
    </p>
    <p style="margin-left:50px;">
        <asp:Label ID="lbl_success" runat="server" ForeColor="Green"></asp:Label>
<br /><br />
        <asp:Button ID="Button3" runat="server" Text="CAPTURE INQUIRY" />
    </p>
    <p style="margin-left:50px;">
        &nbsp;</p>
    </div>
</asp:Content>

