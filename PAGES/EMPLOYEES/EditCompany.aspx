﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditCompany.aspx.vb" Inherits="PAGES_EMPLOYEES_EditCompany" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <section style="min-height:400px;padding-bottom:24px;padding-left:50px;margin-top:20px;height:auto;margin-right:5px;margin-left:5px;width:600px;"> <h3>
        EDIT EXTERNAL ORGANIZATION</h3>
        <p>
            Select &quot;Edit&quot; on the row that you want to edit<asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="CompanyID" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="655px">
            <Columns>
                <asp:CommandField ShowEditButton="True" />
                <asp:TemplateField HeaderText="Organization ID" InsertVisible="False" SortExpression="CompanyID">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("CompanyID") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("CompanyID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name" SortExpression="CompanyName">
                    <EditItemTemplate>
                        <asp:TextBox ID="txt_company" runat="server" Text='<%# Bind("CompanyName") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Field Cannot be blank" ControlToValidate="txt_company"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Address" SortExpression="CompanyAddress">
                    <EditItemTemplate>
                        <asp:TextBox ID="txt_companyAdd" runat="server" Text='<%# Bind("CompanyAddress") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Field cannot be blank" ControlToValidate="txt_companyAdd"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("CompanyAddress") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Telephone" SortExpression="CompanyTelephone">
                    <EditItemTemplate>
                        <asp:TextBox ID="txt_companyTel" runat="server" Text='<%# Bind("CompanyTelephone") %>' MaxLength="10"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Phone Number format incorrect" ControlToValidate="txt_companyTel" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Field cannot be blank" ControlToValidate="txt_companyAdd"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("CompanyTelephone") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Industry" SortExpression="Industry">
                    <EditItemTemplate>
                        <asp:TextBox ID="txt_industry" runat="server" Text='<%# Bind("Industry") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Field cannot be blank" ControlToValidate="txt_industry"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("Industry") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" DeleteCommand="DELETE FROM [Company] WHERE [CompanyID] = ?" InsertCommand="INSERT INTO [Company] ([CompanyID], [CompanyName], [CompanyAddress], [CompanyTelephone], [Industry]) VALUES (?, ?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT * FROM [Company]" UpdateCommand="UPDATE [Company] SET [CompanyName] = ?, [CompanyAddress] = ?, [CompanyTelephone] = ?, [Industry] = ? WHERE [CompanyID] = ?" FilterExpression="CONVERT([CompanyID], 'System.String') LIKE '{0}'">
            <DeleteParameters>
                <asp:Parameter Name="CompanyID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="CompanyID" Type="Int32" />
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="CompanyAddress" Type="String" />
                <asp:Parameter Name="CompanyTelephone" Type="String" />
                <asp:Parameter Name="Industry" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="CompanyAddress" Type="String" />
                <asp:Parameter Name="CompanyTelephone" Type="String" />
                <asp:Parameter Name="Industry" Type="String" />
                <asp:Parameter Name="CompanyID" Type="Int32" />
            </UpdateParameters>
            <FilterParameters>
                    <asp:ControlParameter Name="CompanyID" ControlID="txt_filter" PropertyName="Text" />
    </FilterParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        Filter by Organization ID: <asp:TextBox ID="txt_filter" runat="server"></asp:TextBox>
    &nbsp;<asp:Button ID="Button4" runat="server" Text="FILTER" />
    </p>
    <p>
        &nbsp;</p></section>
</asp:Content>

