﻿Imports System.Data.OleDb
Imports System.Web.Configuration

Partial Class PAGES_EMPLOYEES_CreateInquiry
    Inherits System.Web.UI.Page

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim conn As OleDbConnection
        conn = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn.Open()

        Dim myCommand As OleDbCommand
        Dim insertCmd As String

        Dim dt As DateTime = Convert.ToDateTime(Calendar1.SelectedDate.ToShortDateString())
        Dim ts As TimeSpan = New TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second)
        dt = dt.Add(ts)


        insertCmd = "INSERT into Inquiry(DateCreated, EmployeeID, CustomerID) values(@date, @empid, @custid);"
        myCommand = New OleDbCommand(insertCmd, conn)

        myCommand.Parameters.Add("@date", OleDbType.Date)
        myCommand.Parameters("@date").Value = dt.ToString()

        myCommand.Parameters.Add("@empid", OleDbType.VarChar)
        myCommand.Parameters("@empid").Value = ddl_employee.SelectedValue

        myCommand.Parameters.Add("@custid", OleDbType.Integer)
        myCommand.Parameters("@custid").Value = ddl_customer.SelectedValue


        Try
            myCommand.ExecuteNonQuery()
            lbl_success.Text = "Inquiry successfully created."
        Catch ex As OleDbException
            lbl_success.Text = "ERROR: Could not add record, please ensure the fields are correctly filled out"
        End Try

        'Get last inserted Inquiry ID to put into the Inq_Req table


        Dim selectCmd As String = "SELECT @@IDENTITY;"
        myCommand = New OleDbCommand(selectCmd, conn)


        Dim newID As String = CInt(myCommand.ExecuteScalar())
        myCommand.Connection.Close()






        'Create inquiry_request associative entity

        Dim conn2 As OleDbConnection
        conn2 = New OleDbConnection(ConfigurationManager.ConnectionStrings("AccessDB").ConnectionString)
        conn2.Open()


        Dim myCommand2 As OleDbCommand
        Dim alterCmd2 As String


        alterCmd2 = "INSERT INTO Inquiry_Request(RequestID, InquiryID) VALUES(@ReqID, InqID);"
        myCommand2 = New OleDbCommand(alterCmd2, conn2)

        myCommand2.Parameters.Add("@ReqID", OleDbType.Integer)
        myCommand2.Parameters("@ReqID").Value = ddl_topic.SelectedValue
        myCommand2.Parameters.Add("@InqID", OleDbType.Integer)
        myCommand2.Parameters("@InqID").Value = CInt(newID)


        Try
            myCommand2.ExecuteNonQuery()
        Catch ex As OleDbException
            lbl_success.Text = "ERROR: Back-end error editing inquiries."
        End Try

        myCommand2.Connection.Close()
    End Sub

   


    Protected Sub Calendar1_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar1.SelectionChanged
        TextBox1.Text = Calendar1.SelectedDate.ToShortDateString
        TextBox1.Text += " " + DateTime.Now.ToShortTimeString()
    End Sub


    Private Sub BindCalendar()
        Calendar1.SelectedDate = DateTime.Now
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        BindCalendar()
    End Sub

    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        ddl_customer.DataBind()
        ddl_employee.DataBind()
        ddl_topic.DataBind()

    End Sub
End Class
